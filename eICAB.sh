#!/bin/bash
function main() {
  if [[ -z $EXPRESS_HOME ]]; then
    echo "Please set EXPRESS variable pointing to eICAB repository !"
    echo "Exiting"
    exit 1
  fi

  if [[ $# -eq 0 ]]; then
    help
    exit 0
  fi
  check_argument "$@"

  template_path=$EXPRESS_HOME/MNI
  models_path=$EXPRESS_HOME/weights/${version}/
  express_cw.py "$tof" \
    "$output" \
    ${overwrite} \
    -r "$resolution" \
    ${simple_segmentation} \
    --device "$device" \
    -t "$template_path" \
    -mp "$models_path" \
    -pp \
    -vs "$version" \
    --experimental_prediction \
    -vv
}

function check_argument() {
  overwrite=""
  resolution="0.625"
  simple_segmentation=""
  device="cpu"
  version="labels_18_236"
  while (("$#")); do
    case "$1" in
    -t | --tof)
      tof=$2
      shift 2
      ;;
    -o | --output)
      output=$2
      shift 2
      ;;
    -f | --overwrite)
      overwrite="-f"
      shift 1
      ;;
    -r | --resolution)
      resolution=$2
      shift 2
      ;;
    -s | --simple_segmentation)
      simple_segmentation="-ss"
      shift 1
      ;;
    -d | --device)
      device=$2
      shift 2
      ;;
    -e | --version)
      version=$2
      shift 2
      ;;
    -h | --help)
      help
      exit 1
      ;;
    *)
      wrong_args
      exit 1
      ;;
    esac
  done
}

function wrong_args() {
  echo "usage: $0 [-t|--tof] [-o|--output]" >&2
}

function help() {
  echo "eICAB.sh takes a TOF-MRA as input and an output directory"
  echo "eICAB.sh"
  echo "-t|--tof                  (MANDATORY) ToF MRA image [ 3D image | .nii/ .nii.gz ]."
  echo "-o|--output               (MANDATORY) Defines the output folder."
  echo "-f|--overwrite            Force overwriting of the output files."
  echo "-r|--resolution           Isotropic resampling resolution in mm [ Xmm Ymm Zmm ] (default: 0.625 mm)."
  echo "-s|--simple_segmentation  If flagged, will only output segmentation maps."
  echo "-d|--device               Device used for neural network inference [ cpu | cuda ] (default: cpu)."
  echo "-e|--version              Version used [labels_18, labels_18_236] (default: labels_18_236)."
}

main "$@"
