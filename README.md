# Express Intracranial Arteries Breakdown (eICAB)

This repository enables automated segmentation and labeling of the main intracranial arteries on MR angiography images.
Further details can be found in the following publication:

[eICAB: A novel deep learning pipeline for Circle of Willis multiclass segmentation and analysis](https://doi.org/10.1016/j.neuroimage.2022.119425)

Dumais, F., Caceres, M. P., Janelle, F., Seifeldine, K., Arès-Bruneau, N., Gutierrez, J., Bocti, C., Whittingstall, K.
eICAB: A novel deep learning pipeline for Circle of Willis multiclass segmentation and
analysis.
NeuroImage 119425 (2022) doi:10.1016/j.neuroimage.2022.119425.

## Getting Started (Option 1, Recommended)

### eICAB installation

* To install eICAB, we strongly recommend downloading Docker https://docs.docker.com/get-docker/
* If you use the Docker image, you don't have to clone the project. You just have to install Docker on your
  computer and you are good to go.
* Tested with Docker Version `20.10.7`

### eICAB launch

Notes: A new Docker image supporting CUDA application will soon be released. The Docker image currently works only on
CPU.

Begin by running eICAB on the sample MRA image (TOF.nii.gz) provided
here (https://usherbrooke-my.sharepoint.com/:u:/g/personal/dumf2406_usherbrooke_ca/EQ68PC9R5j1OmWD5-fd-d38B6Vikc3431cdqbRB6zTafgA?e=lKa262):

```
docker run 
-v /absolute/path/to/TOF.nii.gz:/TOF.nii.gz 
-v /absolute/path/to/output:/output 
--rm -it felixdumais1/eicab -t /TOF.nii.gz -o /output -f
```

where `-t` is the input image and `-o` is the output folder
(type the following command to see all additional parameters).

```
docker run --rm -it felixdumais1/eicab
```

IMPORTANT: Input MRA images must be raw (unprocessed, not skull stripped) images. Upon completion (~10 mins runtime),
eICAB will generate the following:

1. The file that ends with `*_eICAB_CW.nii.gz` consists of a segmented Circle of Willis (CW) annotated with a maximum of
   18 labels corresponding to each major artery (see below for label names). By default, the resolution is 0.625 mm
   isotropic (use `-r` option to resample to desired resolution).
2. `*_resampled.nii.gz` is the resampled MRA in the same space as the eICAB prediction. It should be noted that the MRA
   is reoriented and autoboxed.
3. The folder `original_space` contains files in the original space (before resampling).
4. The folder `nn_space` contains files in the space used for eICAB inference.
5. The folder `metric_space` contains files used for computing diameter and volume of each artery. Results can be found
   in the file named `*_SUMMARY.csv` in the folder `metric_space/*_CircleOfWillis`. An example is shown below. Volumes
   are in mm³ and Diameters in mm.
6. A basic binary per label segmentation confidence score based on centerline analysis and prior CW connectivity
   knowledges is provided in the summary file. This helps to filter hypoplastic, aplastic or rare artery conformities. A
   bad confidence score does not mean that the segmentation is wrong, but further human analysis is needed to assess the
   validity of the segmentation.  
   ![alt text](images/summary.png)

### eICAB labels

eICAB automatically segments and labels the Internal Carotid Arteries (ICA), Basilar Artery (BA), Anterior Communicating
Artery (AComm), Anterior Cerebral Arteries (ACA), Middle Cerebral Arteries (MCA), Posterior Communicating Arteries (
PComm), Posterior Cerebral Arteries (PCA), Superior Cerebellar Arteries (SCA) and Anterior Choroidal Arteries (AChA).

Labels 15 to 18 are still experimental, but were annotated and trained using the same scheme as label 1 to 14 (see
article).

| Arteries | Left | Right |
|----------|------|:-----:|
| ICA      | 1    |   2   |
| ACA-A1   | 5    |   6   |
| MCA-M1   | 7    |   8   |
| PComm    | 9    |  10   |
| PCA-P1   | 11   |  12   |
| PCA-P2   | 13   |  14   |
| SCA      | 15   |  16   |
| AChA     | 17   |  18   |

BAS = 3, AComm = 4

## Getting Started (Option 2, <u>not</u> Recommended)

### Dependencies

* ANTs (https://github.com/ANTsX/ANTs.git)
    * Tested with ANTs Version: `v2.3.5.post79-gdb98de3`
* AFNI (https://afni.nimh.nih.gov/)
    * Tested with AFNI Version `AFNI_22.0.02 'Hadrian'`

### eICAB download

All the source code is in [vessel_segmentation](vessel_segmentation).<br />

The main script [express_cw.py](scripts/express_cw.py) is in the folder [scripts](scripts) and is called by a bash
wrapper [eICAB.sh](eICAB.sh) (see section [eICAB launch](#eicab-launch)).

To clone the repository, make sure that `git-lfs` is installed:

* For Ubuntu

``` 
sudo apt update
sudo apt install git-lfs
git lfs install
```

* For Mac OS

``` 
brew install git-lfs
git lfs install
```

In you bashrc (Ubuntu) or bash_profile (MacOS), create an environment variable:

``` 
export EXPRESS_HOME="dir/to/vessel_segmentation_snaillab"
export PATH="$EXPRESS_HOME:$PATH"
```

Then, do `git lfs clone https://gitlab.com/FelixDumais/vessel_segmentation_snaillab.git`.
You just have to do `git-lfs` once. After, you can use `git` as usual.

### eICAB installation

* Create a directory <em>data</em> at the root of your project.
* Create a directory <em>data/demo1</em> and include <em>TOF.nii.gz</em>.
* Create a new environment in python3.7.

``` 
virtualenv eICAB_env --python=python3.7
source eICAB_env/bin/activate
```

Install the package in your new environment to have access to each script from everywhere and to install all the
dependencies

```
pip install -e path/to/project
```

Script tested with:

| Torch  |  Tested   |
|--------|:---------:|
| 1.5.0  |    CPU    |
| 1.7.1  |    CPU    |
| 1.7.1  | CUDA 11.0 |
| 1.11.1 |    CPU    |
| 1.11.1 | CUDA 11.3 |

Torch not included in the requirements

```
pip3 install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cu113
```

### eICAB launch

IMPORTANT: Input TOF-MRA nifti images must be raw TOF unprocessed and not skull stripped images.

```
eICAB.sh -t path/to/TOF.nii.gz -o path/to/results
```

### Common technical problems

1. Due to `git-lfs` usage, make sure that the [weights](weights) are fully downloaded.
   The size of [labels_18](weights/labels_18) weights are about 96M and the size
   of [labels_18_236](weights/labels_18_236) are about 382M.
2. You might have problems cloning the repository due to `git-lfs`. If so try:

```
git lfs clone --filter blob:limit=1m https://gitlab.com/FelixDumais/vessel_segmentation_snaillab.git
```

3. If you do not use the singularity container make sure that `ANTS` and `AFNI` are well installed and the versions
   matched the ones specified previously.
   We know that it might cause problems with `nipype` package.
4. Developed and tested with `Python3.7` on Ubuntu 18.04 and 20.04.

## eICAB versions

With `eICAB.sh` there is an argument called `--version` that comes with 2 options, namely `labels_18`
and `labels_18_236`. We use `labels_18_236` by default.

* `labels_18` is doing inference with the exact neural network weights presented in the paper trained on 116 subjects.
* `labels_18_236` is doing inference with a network trained with 236 MRA.

## Pipeline issues

1. In our experience, the main reason eICAB fails is that it cannot properly register the input image to atlas space
   during the patch creation step. We are actively investigating ways to circumvent this (feedback is of course very
   welcomed).

## Citations

If you use eICAB for your projects, please cite:

1. <b>SEGMENTATION</b>:  Dumais, F. et al. eICAB: A novel deep learning pipeline for Circle of Willis multiclass
   segmentation and analysis. NeuroImage 260, 119425 (2022).
2. <b>DIAMETER ESTIMATES</b>: Bizeau, A. et al. Stimulus-evoked changes in cerebral vessel diameter: A study in healthy
   humans.
   J Cereb Blood Flow Metab 38, 528–539 (2018).

## Notice

This software is provided "as is" and without any warranties for any particular purpose.




