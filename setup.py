"""A setuptools based setup module.
See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""
# pylama:skip=1
# To use a consistent encoding
from os import path
from glob import glob

# Always prefer setuptools over distutils
from setuptools import setup, find_packages

here = path.abspath(path.dirname(__file__))
# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.
setup(
    # There are some restrictions on what makes a valid project name
    # specification here:
    # https://packaging.python.org/specifications/core-metadata/#name
    name="vessel_segmentation",  # Required
    # Versions should comply with PEP 440:
    # https://www.python.org/dev/peps/pep-0440/
    #
    # For a discussion on single-sourcing the version across setup.py and the
    # project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version="v2.0",  # Required
    # This is a one-line description or tagline of what your project does. This
    # corresponds to the "Summary" metadata field:
    # https://packaging.python.org/specifications/core-metadata/#summary
    description="Vessel segmentation package",  # Required
    # This should be your name or the name of the organization which owns the
    # project.
    author="Félix Dumais",  # Optional
    # This should be a valid email address corresponding to the author listed
    # above.
    author_email="felix.dumais@usherbrooke.ca",  # Optional
    # Classifiers help users find your project by categorizing it.
    #
    # For a list of valid classifiers, see
    # https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[  # Optional
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        "Development Status :: 5 - Production",
        # Indicate who your project is intended for
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Pytorch utilities",
        # Pick your license as you wish
        "License :: Imeka licence.",
        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        "Programming Language :: Python :: 3.7",
    ],
    # You can just specify package directories manually here if your project is
    # simple. Or you can use find_packages().
    #
    # Alternatively, if you just want to distribute a single Python file, use
    # the `py_modules` argument instead as follows, which will expect a file
    # called `my_module.py` to exist:
    #
    #   py_modules=["my_module"],
    #
    packages=find_packages(exclude=["data"]),  # Required
    # This field lists other packages that your project depends on to run.
    # Any package you put here will be installed by pip when your project is
    # installed, so they must be valid existing projects.
    #
    # For an analysis of "install_requires" vs pip's requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=[
        "dipy==1.1.1",
        "itk==5.1.0.post3",
        "matplotlib==3.2.1",
        "multiprocess==0.70.10",
        "nibabel==3.2.2",
        "nipype==1.8.0",
        "numpy==1.21.6",
        "opencv-python==4.2.0.34",
        "pandas==1.0.4",
        "raster-geometry==0.1.4.1",
        "scikit-image==0.16.2",
        "scikit-learn==0.23.1",
        "scipy==1.7.3",
        "tqdm==4.46.1",
        "xlrd==1.2.0",
        "XlsxWriter==1.2.9",
    ],
    # Optional
    scripts=glob("scripts/*.py") + glob("test/*.py"),
)
