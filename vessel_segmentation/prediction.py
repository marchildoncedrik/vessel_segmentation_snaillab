import argparse
import logging
import os
import sys
from collections import OrderedDict
from os.path import join
from typing import Tuple, List

import nibabel as nib
import numpy as np
import torch
from dipy.denoise.noise_estimate import piesno
from dipy.denoise.non_local_means import non_local_means as nlmeans
from sklearn.preprocessing import MinMaxScaler

from vessel_segmentation import ArterialEnum
from vessel_segmentation.NeuralNetworks.models.ThreeDUnetDropout import (
    ThreeDUnetDropout,
)
from vessel_segmentation.NeuralNetworks.models.UnetDropout3DIsensee import (
    UnetDropout3DIsensee,
)
from vessel_segmentation.NeuralNetworks.utils import resize_image
from vessel_segmentation.postprocessing import cleanup_markers
from vessel_segmentation.tof_master import extract_vessels_itk


def predict_file(
    type: str,
    model: str,
    array: np.array,
    output: str,
    prefix: str,
    cube: np.array,
    binary_image: np.array,
    voxel_volume: float,
    nii_attributes: nib.nifti1.Nifti1Image,
    device: torch.device,
    attention: bool = False,
    version: str = "labels_14",
    post_processing: bool = True,
    **kwargs,
):
    """Use this function to predict the CW

    Args:

        type (str): Type of the prediction
        model (str): Path to model
        array (np.array): Whole brain image
        output (str): Output folder
        prefix (str): Prefix of the file name
        cube (np.array): Binary cube around the CW
        binary_image (np.array): Binarize vessel of the CW
        voxel_volume (float): Volume of 1 voxel (mm³)
        nii_attributes (nibabel.nifti1.Nifti1Image): Whole brain image file
        device (str): Device to use
        attention (bool): Flag if you want to use an attention model
        version (str): Used version
        post_processing (bool): Flag if you want to use an post processing

    Returns:
        markers (np.array): Markers predicted
        prob_all (np.array): Softmax output

    """
    logging.info(f"   +++ Predicting on {type} image")
    markers_file = join(output, f"{prefix}_{type}_markers.nii.gz")

    map_label = {"labels_14": 15, "labels_16": 17, "labels_18": 19, "labels_18_236": 19}
    markers, prob_all = predict_markers(
        model,
        array,
        cube,
        device=device,
        possible_label=map_label[version],
        version=version,
        **kwargs,
    )

    if attention:
        logging.info("Applying attention model")
        markers *= binary_image.astype(int)

    if post_processing:
        logging.info("Applying post processing")
        markers = cleanup_markers(markers, voxel_volume)

    nib.Nifti1Image(markers, nii_attributes.affine, nii_attributes.header).to_filename(
        markers_file
    )

    return markers, prob_all


def predict_markers(
    model_path: str,
    data: np.ndarray,
    mask: np.ndarray,
    device: torch.device,
    possible_label: int = 15,
    version: str = "labels_14",
    **kwargs,
) -> Tuple[np.ndarray, np.ndarray]:
    """Predict the markers.

    Args:
        model_path (str): Pytorch state dict file.
        data (np.ndarray): Input image.
        mask (np.ndarray): Mask use to constrain the prediction.
        device (torch.device): Device to use for the prediction.
        possible_label (int, optional): Number of classes in the model. Defaults to 15.

    Raises:
        ValueError: If the model input is not a 5D tensor for 3D prediction.

    Returns:
        Tuple[np.ndarray, np.ndarray]: Model segmentation and model probabilities.
    """

    mask = mask.astype(bool)
    (sag, coro, axial) = np.nonzero(mask)
    sag_min = sag.min()
    sag_max = sag.max()
    coro_min = coro.min()
    coro_max = coro.max()
    axial_min = axial.min()
    axial_max = axial.max()

    sag_orig = sag_max - sag_min
    coro_orig = coro_max - coro_min
    axial_orig = axial_max - axial_min

    # 2) Normalize image
    logging.info(" +++: Normalizing the image.")

    if version == "labels_14":
        cube_tof = data[sag_min:sag_max, coro_min:coro_max, axial_min:axial_max]
        cube_tof = scale_data(cube_tof)
        model = ThreeDUnetDropout(possible_label)
        model.load_state_dict(torch.load(model_path, map_location=device))

    elif version in ["labels_16", "labels_18", "labels_18_236"]:
        if version == "labels_18_236":
            cube_tof = data[sag_min:sag_max, coro_min:coro_max, axial_min:axial_max]
            cube_tof = scale_data(cube_tof)

        else:
            data_scaled = scale_data(data)
            cube_tof = data_scaled[
                sag_min:sag_max, coro_min:coro_max, axial_min:axial_max
            ]

        # cube_tof = scale_data(cube_tof)
        state_dict = torch.load(model_path, map_location=device)["model_state_dict"]
        new_state_dict = OrderedDict()
        for k, v in state_dict.items():
            name = k[7:]  # remove `module.`
            new_state_dict[name] = v
        # load params
        model = UnetDropout3DIsensee(
            num_classes=possible_label,
            init_weights=False,
            drop_value=0.3,
            channels=kwargs["isensee_channels"],
        )
        model.load_state_dict(new_state_dict)

    else:
        raise NotImplementedError("Version {} not implemented".format(version))

    model.to(device)
    # model = torch.jit.load(model_path, map_location=device)
    tensor_size = list(model.parameters())[0].shape
    if len(tensor_size) != 5:
        raise ValueError("Only 3d model are supported.")

    logging.info(f"Number of possible labels: {possible_label}")

    # Padding (or croping) the cube to have the right size for the prediction
    cube_resized = resize_image(cube_tof, shape=96)

    image = torch.from_numpy(cube_resized).to(device)
    if version == "labels_14":
        # Orig orient (SAG, CORO, AXIAL) -> Permute orientation (CORO, AXIAL, SAG)
        image = image.permute((1, 2, 0)).contiguous()

    image = image[None, None]
    prediction, probs, probs_all = predict3d(model, image)

    if version == "labels_14":
        # Permute orientation (CORO, AXIAL, SAG) -> Orig orient (SAG, CORO, AXIAL)
        prediction_tensor = prediction[0].permute((2, 0, 1)).cpu().numpy()

        # Permute orientation (Channels, CORO, AXIAL, SAG) -> Orig orient (Channels, SAG, CORO, AXIAL)
        probability_tensor_all = probs_all[0].permute((0, 3, 1, 2)).cpu().numpy()
        shape_probs_all = probability_tensor_all.shape

    else:
        prediction_tensor = prediction[0].cpu().numpy()
        probability_tensor_all = probs_all[0].cpu().numpy()
        shape_probs_all = probability_tensor_all.shape

    # Put the cube prediction in the original space
    final_prediction = np.zeros_like(data)
    cube_cropped = resize_image(
        prediction_tensor,
        (sag_orig, coro_orig, axial_orig),
    )
    final_prediction[
        sag_min:sag_max, coro_min:coro_max, axial_min:axial_max
    ] = cube_cropped

    final_prediction = final_prediction.astype(int)

    # Put the cube softmax in the original space
    final_probability_all = np.zeros((shape_probs_all[0],) + data.shape).astype(
        data.dtype
    )

    cube_cropped_prob_all = resize_image(
        probability_tensor_all,
        (sag_orig, coro_orig, axial_orig),
    )
    final_probability_all[
        :, sag_min:sag_max, coro_min:coro_max, axial_min:axial_max
    ] = cube_cropped_prob_all
    final_probability_all = final_probability_all.astype(float)

    return final_prediction, final_probability_all


def predict3d(
    model: torch.nn.Module,
    image: torch.Tensor,
) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
    """Predict volume with 3D model (CNN).

    Args:
        model (torch.nn.Module): Torch model (not JIT).
        image (torch.Tensor): Input image.

    Returns:
        Tuple[torch.Tensor, torch.Tensor]: Prediction label, probabilities.
    """
    model.eval()
    image = image.to(dtype=torch.float32)

    with torch.no_grad():
        prediction: torch.Tensor = model(image)
    probs_all = prediction.softmax(dim=1)
    prediction = probs_all.argmax(dim=1)
    probs, _ = probs_all.max(dim=1)
    return prediction, probs, probs_all


def scale_data(data: np.ndarray) -> np.ndarray:
    """Rescale data from 0-1 range inside the mask area.

    Args:
        data (np.ndarray): Data to rescale.

    Returns:
        np.ndarray: Data rescaled.
    """
    # Flattening Data
    shape = data.shape
    data_flat = data.flatten()

    # Scaling
    sklearn_scaler = MinMaxScaler()
    data_flat = data_flat.reshape(-1, 1)

    data_robust = sklearn_scaler.fit_transform(data_flat)
    data_robust = data_robust.reshape(-1)

    # Reshaping and saving
    return data_robust.reshape(shape)


def dipy_nlmeans(data: np.array, std: float = 0, mask: np.array = None) -> np.array:
    """Apply nlmeans denoising of the input.

    Args:
        data (np.array): image to denoise.
        std (float, optional): Standard deviation for nlmeans computation. Defaults to 0.
        mask (np.array, optional): Mask used in nlmeans. Defaults to None.

    Returns:
        np.array: Image denoised.
    """

    logging.info("===> NLMEANS")

    if std is 0:
        logging.info("    Computing sigma with piesno")
        sigma = piesno(data, N=1, return_mask=False)
        logging.info(f"    Sigma = {sigma}")
        sigma = np.mean(sigma)
        logging.info(f"    Sigma = {sigma}")

    else:
        logging.info(f"    Sigma = {std}")
        sigma = std

    img_denoised = nlmeans(data, sigma, mask=mask)

    logging.info("===> Image denoised")

    return img_denoised


def majority_vote(markers_list_prob: List[np.array]) -> np.array:
    """Do a majority vote with a list of array corresponding to markers probability.

    Args:
        markers_list_prob (list): List containing array of many different NN softmax.

    Returns:
        prediction (np.array): prediction of the majority vote.
    """

    markers_sum = sum(markers_list_prob)
    return np.argmax(markers_sum, axis=0)


def neuronal_network_combination(
    data: np.ndarray,
    args: argparse.Namespace,
    prefix: str,
    cube: np.ndarray,
    binary_image: np.ndarray,
    voxel_volume: float,
    nii_attributes,
    resample_file,
    markers_file,
    markers_file_for_lab,
    output_directory: str,
    experimental_artery=False,
):
    """
    Makes the different combination of Neuronal Networks
    Args:
        data (np.array): Image
        args(argparse.Namespace): Command line arguments.
        prefix: Image prefix
        cube: cube image
        binary_image: binary image
        voxel_volume: Image resolution
        nii_attributes: NIFIT affine and header attributes
        resample_file: Data file name
        markers_file: Markers file name
        markers_file_for_lab: Markers file name with all output arteries
        experimental_artery: If flagged markers will have all arteries

    Returns:
        markers (np.array): Output CW predictions

    """

    kwargs = {
        "output": output_directory,
        "prefix": prefix,
        "cube": cube,
        "binary_image": binary_image,
        "voxel_volume": voxel_volume,
        "nii_attributes": nii_attributes,
        "device": args.device,
        "attention": args.attention,
        "version": args.version,
        "post_processing": args.post_processing,
    }
    resampled_array = data

    models_file = [f for f in os.listdir(args.models_path) if ".pt" in f]

    if args.version == "labels_14":
        probabilities = []

        if any("Raw_model.pt" in x for x in models_file):
            markers, probability = predict_file(
                type="raw",
                model=join(args.models_path, "Raw_model.pt"),
                array=resampled_array,
                **kwargs,
            )
            probabilities.append(probability)

        if any("Vesselness_model.pt" in x for x in models_file):
            vessel_file = join(output_directory, f"{prefix}_vessels.nii.gz")
            vesselness = extract_vessels_itk(resample_file, vessel_file)
            markers, probability = predict_file(
                type="vessel",
                model=join(args.models_path, "Vesselness_model.pt"),
                array=vesselness,
                **kwargs,
            )
            probabilities.append(probability)

        if any("NLMeans_model.pt" in x for x in models_file):
            nlmeans_image = dipy_nlmeans(resampled_array)
            nib.Nifti1Image(
                nlmeans_image, nii_attributes.affine, nii_attributes.header
            ).to_filename(join(output_directory, f"{prefix}_nlmeans.nii.gz"))
            markers, probability = predict_file(
                type="nlmeans",
                model=join(args.models_path, "NLMeans_model.pt"),
                array=nlmeans_image,
                **kwargs,
            )
            probabilities.append(probability)

        if any("Binary_model.pt" in x for x in models_file):
            markers, probability = predict_file(
                type="binary",
                model=join(args.models_path, "Binary_model.pt"),
                array=binary_image,
                **kwargs,
            )
            probabilities.append(probability)

        if len(probabilities) > 1:
            markers = majority_vote(probabilities)
            if args.attention:
                logging.info("Applying attention model")
                markers *= binary_image.astype(int)

            if args.post_processing:
                markers = cleanup_markers(markers, voxel_volume)

        elif len(probabilities) == 0:
            raise IOError("No neural network model weight provided")

    elif args.version in ["labels_16", "labels_18", "labels_18_236"]:
        files = os.listdir(args.models_path)
        files = [f for f in files if os.path.isfile(join(args.models_path, f))]
        probabilities = []
        if args.version == "labels_18_236":
            kwargs["isensee_channels"] = 2
        else:
            kwargs["isensee_channels"] = 1

        for file in files:
            try:
                markers, probability = predict_file(
                    type=f"raw_{file[:-3]}",
                    model=join(args.models_path, file),
                    array=resampled_array,
                    **kwargs,
                )
            except Exception as e:
                logging.info(
                    "Problem with neural network prediction. Make sure all models in model"
                    "path are of type 3DUnetIsensee."
                )
                logging.info(e)
                sys.exit(1)

            probabilities.append(probability)

        markers = majority_vote(probabilities)
        if args.attention:
            logging.info("Applying attention model")
            markers *= binary_image.astype(int)
        if args.post_processing:
            logging.info("Applying post processing")
            markers = cleanup_markers(markers, voxel_volume)

    else:
        raise ValueError(
            f"Accepted version are currently labels_14, labels_16, labels_18 & labels_18_236. Got {args.version}"
        )

    nib.Nifti1Image(markers, nii_attributes.affine, nii_attributes.header).to_filename(
        markers_file_for_lab
    )

    if not experimental_artery:
        logging.info("Predicting 14 labels")
        for artery in [
            ArterialEnum.RSCA.value,
            ArterialEnum.LSCA.value,
            ArterialEnum.LAChA.value,
            ArterialEnum.RAChA.value,
        ]:
            markers[markers == artery] = 0

    nib.Nifti1Image(markers, nii_attributes.affine, nii_attributes.header).to_filename(
        markers_file
    )

    return markers
