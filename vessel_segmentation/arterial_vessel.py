# -*- coding: utf-8 -*-
import collections
from collections import defaultdict
import itertools as it
import logging
import math
from typing import Sequence, Tuple, Union, List

import numpy as np
import networkx as nx
import pandas as pd
from scipy.ndimage.morphology import distance_transform_edt
from scipy.spatial import distance as dist
from scipy.ndimage import center_of_mass
from scipy import stats
from skimage.graph import route_through_array
from skimage.morphology import label as give_labels
from tqdm import tqdm


def create_closest_point_lut(vessels_data, mask, return_map=False):
    closest_values = np.zeros_like(vessels_data)
    distinct_points = np.zeros_like(vessels_data)

    # Finding all points in the sub_image where an arterial voxel exists
    locations = np.zeros((np.count_nonzero(vessels_data), 3))
    i = 0
    print("===> Indexing Vascular Voxels")
    for index, value in tqdm(np.ndenumerate(vessels_data), total=vessels_data.size):
        if value != 0:
            locations[i] = index
            distinct_points[index] = i + 1
            i += 1

    print("===> Finding Closest Vascular Voxel")
    for index, value in tqdm(np.ndenumerate(mask), total=mask.size):
        # If the index is not a mask voxel then we add Not a Number (nan)
        if value == 0:
            closest_values[index] = np.nan

        # If the index is a mask voxel we try to compute the nearest vascular distance within a
        # search sphere of given input radius
        else:
            xa = np.zeros((1, 3))
            xa[0] = index
            distances = dist.cdist(xa, locations, metric="euclidean")
            distances = distances.reshape(-1)
            value_index = np.where(distances == np.min(distances))[0][0]
            value_index = locations[value_index].astype(int)
            closest_values[index] = distinct_points[value_index[0], value_index[1], value_index[2]]

    closest_lut = {}
    print("===> Creating Vascular Voxels Wise Dependency LUT")
    for index, value in tqdm(np.ndenumerate(distinct_points), total=distinct_points.size):
        if value != 0:
            closest_lut[index] = np.where(closest_values == value)

    if return_map:
        return closest_lut, closest_values
    else:
        return closest_lut


def create_lookup_table(dist_sq_values):
    """
    For each offset from the origin (dx, dy, dz), and each radius-squared, r_sq
    find the smallest radius-squared, r1_sq, such that a ball of radius r1
    centered at (dx, dy, dz) includes a ball of r_sq centered at the origin.
    These balls refer to a 3D Integer grid. The set of (dx, dy, dz) points
    considered is a cube center at the origin.

    The code is based on this article:

    Exact medial axis with euclidean distance, E. Remy and E. Thiel,

    :param dist_sq_values
        A list of all possible distances values.
    :return:
        A look-up table to compare distance for redundant points.
    """
    # Create a Look-up table based on direction v (all directions are in 3D)
    # and the possible radius from the structure.
    side = scan_cube(1, 0, 0, dist_sq_values)
    diagonal = scan_cube(1, 1, 0, dist_sq_values)
    corner = scan_cube(1, 1, 1, dist_sq_values)

    return side, diagonal, corner


def scan_cube(dx, dy, dz, dist_sq_values):
    """
    For a list of r_sq, find the smallest r1_sq values such that a "ball" of
    radius r1 centered at (dx, dy, dz) includes the origin.

    The code is based on this article:

    Exact medial axis with euclidean distance, E. Remy and E. Thiel,

    A shape point p is the centre of a maximal disk if there is no other
    shape point q such that the ball BK1(q, DT[q]) entirely covers the
    ball BK1(p, DT[p]). (see figure 2 in paper to understand)

    :param dx
        Direction in x.
    :param dy
        Direction in y.
    :param dz
        Direction in z.
    :param dist_sq_values
        A list of all possible distances values
    :return
        A look-up table in a certain direction.
    """
    r1_radii = np.zeros((len(dist_sq_values),), dtype=np.int16)

    # Obtain the direction vector v.
    vx_abs = -int(math.fabs(dx))
    vy_abs = -int(math.fabs(dy))
    vz_abs = -int(math.fabs(dz))

    # For each possible square radius of the structure
    for t in range(len(dist_sq_values)):

        sq_r = dist_sq_values[t]
        r = 1 + int(math.sqrt(sq_r))
        max_sq_r = 0

        # Iterate on each value of the selected radius
        for k in range(r + 1):
            # Get offset in K
            sq_k = k ** 2
            # Radius k in direction v_z
            sq_rad_k = (k - vz_abs) ** 2

            for j in range(r + 1):
                # Get offset in K and J
                sq_kj = sq_k + j ** 2

                # Check if offset are still within the structure
                if sq_kj > sq_r:
                    continue

                # Radius I (offset between r and kj) in direction v_x
                rad_i = int(math.sqrt(sq_r - sq_kj)) - vx_abs

                # Radius K + Radius J in direction v_y + squared Radius I
                sq_r1 = sq_rad_k + (j - vy_abs) ** 2 + rad_i ** 2

                # Get maximal radius r1 into the template for current t
                if sq_r1 > max_sq_r:
                    max_sq_r = sq_r1

        r1_radii[t] = max_sq_r

    return r1_radii


def is_inside(shape, coord):
    for shape_, coord_ in zip(shape, coord):
        if coord_ < 0 or coord_ >= shape_:
            return False

    return True


def create_connectivity_list(size):
    return [
        (i, j, k)
        for i in range(-size, size + 1)
        for j in range(-size, size + 1)
        for k in range(-size, size + 1)
        if (not (i == j == k == 0))
    ]


def compute_distance_map(data):
    """
    To extract local thickness, the algorithm starts by creating the distance
    map from a binary image (segmentation). And here, we use the scipy EDT
    function (similar to the one used in the Fiji plugin).

    The code is based on this article:

    New algorithms for Euclidean distance transformation on an n-dimensional
    digitized picture with applications," T. Saito and J. Toriwaki, Pattern
    Recognition 27 (1994) 1551-1565.

    :param data
        A binary mask.
    :return:
        A volume with distance value in each voxel of the binary mask.

    """

    return distance_transform_edt(data)


def compute_ridge_distance(edt):
    """
    The next step is to remove redundant points from the distance map to reduce
    the number of voxels to be tested for sphere fitting.

    The code is based on those articles:

    Reverse Distance Transformation and Skeletons Based upon the Euclidean
    Metric For n-Dimensional Binary Pictures, T. Saito and J. Toriwaki,
    IEICE Trans. Inf. & Syst., Vol E77-D, No. 9, Sept. 1994

    Exact medial axis with euclidean distance, E. Remy and E. Thiel,
    Image and Vision Computing 23 (2005) 167-175.

    :param edt
        A volume with distance value in each voxel of the binary mask.
    :return:
        A volume where redundant voxels from input has been removed.
    """

    # Get the rounded integer value of the square max value
    r_sq_max = int(round(np.max(edt) ** 2)) + 1

    # Get square distance for a sphere as an integer for each possible radius.
    dist_sq_values = np.round(np.unique(edt) ** 2).astype(np.int16)

    # Create an index array which associates potential distances with the max.
    dist_sq_index = np.zeros((r_sq_max,), dtype=np.int16)
    ind_ds = 0
    for x in range(r_sq_max):
        if x in dist_sq_values:
            dist_sq_index[x] = ind_ds
            ind_ds += 1

    # Create LUT based on existing distance values in the original mask.
    r_sq_lut = create_lookup_table(dist_sq_values)

    ridge_distance = np.zeros(edt.shape)
    connectivity = create_connectivity_list(1)

    for x, y, z in np.argwhere(edt):
        dist_value = edt[x, y, z]
        redundant_point = False

        # Get the associate index to search in LUT
        sk0_sq_ind = dist_sq_index[int(round(dist_value ** 2))]

        # Loop on each neighbor to check if the current voxel
        # is a redundant voxel in the distance map.
        for n in [
            tuple(e) for e in np.asarray((x, y, z)) - connectivity if is_inside(edt.shape, tuple(e))
        ]:

            # Get the distance of the neighbor in number of voxel (1, 2 or 3),
            # Also reduced by 1, to have LUT index between 0 and 2 (python
            # indexing).
            num_comp = np.sum(np.abs(np.asarray(n) - (x, y, z))) - 1

            # Check if current point (x,y,z) is in the neighbor sphere at
            # position n.
            if int(round(edt[n] ** 2)) >= r_sq_lut[num_comp][sk0_sq_ind]:
                redundant_point = True
                break

        if not redundant_point:
            ridge_distance[x, y, z] = dist_value

    return ridge_distance


def compute_thickness(ridge_distance):
    """
    The next step is to compute the local thickness by fitting a sphere in each
    points that remains in the ridge distance map.

    The code is based on this article:

    A new method for the model-independent assessment of thickness in
    three-dimensional images" T. Hildebrand and P. Rüesgsegger,
     J. of Microscopy, 185 (1996) 67-75.


    :param ridge_distance
        A volume where redundant voxels from input has been removed.
    :return:
        A volume with diameter value in each voxel (Bigger than binary mask).
    """

    w, h, d = ridge_distance.shape
    thickness = np.copy(ridge_distance)

    for i, j, k in np.argwhere(ridge_distance):
        r_squared = int(round(ridge_distance[i, j, k] ** 2))
        r_int = int(math.ceil(ridge_distance[i, j, k]))

        i_start = 0 if i - r_int < 0 else i - r_int
        i_stop = w if i + r_int >= w else i + r_int + 1

        j_start = 0 if j - r_int < 0 else j - r_int
        j_stop = h if j + r_int >= h else j + r_int + 1

        k_start = 0 if k - r_int < 0 else k - r_int
        k_stop = d if k + r_int >= d else k + r_int + 1

        for k1 in range(k_start, k_stop):
            r1_squaredk = (k1 - k) ** 2
            for j1 in range(j_start, j_stop):
                r1_squaredjk = r1_squaredk + (j1 - j) ** 2

                # Check if we are still within the original sphere
                if r1_squaredjk > r_squared:
                    continue

                for i1 in range(i_start, i_stop):
                    r1_squaredijk = r1_squaredjk + (i1 - i) ** 2

                    # Check if we are still within the original sphere
                    if r1_squaredijk <= r_squared:
                        # If the current sphere includes this neighbor and
                        # radius is bigger, modify neighbor value.
                        thickness[i1, j1, k1] = max(r_squared, thickness[i1, j1, k1])

    # This function does not return the masked version of the thickness,
    # because it needs to be cleaned with clean thickness function. But when
    # it is written on the disk, the output is masked using the original
    # volume. (This is done to be identical to BoneJ plugin output).
    return 2.0 * np.sqrt(thickness)


def compute_clean_thickness(mask, thickness, image_resolution):
    """
    Next step is to clean the border of local thickness images.

    The code is based on this article:

    Clean thickness code from Local thickness OptiNav.
    http://www.optinav.com/Local_Thickness.htm

    :param mask
        A binary mask.
    :param thickness
        A volume with diameter value in each voxel (Bigger than binary mask).
    :param image_resolution
        Resolution of the original mask data.
    :return:
        A volume with approximate diameter value in mm, same size as the
        original mask.

    """
    connectivity = create_connectivity_list(1)

    # Create a flag array (0 = background, -1 = border, Thickness = inside)
    flag_array = np.copy(thickness)
    for x, y, z in np.argwhere(thickness):
        for n in [
            tuple(e)
            for e in np.asarray((x, y, z)) - connectivity
            if is_inside(thickness.shape, tuple(e))
        ]:

            # Set this point to border flag if one of neighbors is background.
            if thickness[n] == 0:
                flag_array[x, y, z] = -1
                break

    result_array = np.copy(thickness)
    for x, y, z in np.argwhere(flag_array == -1):
        positive_sum = 0
        nb_point = 0
        for n in [
            tuple(e)
            for e in np.asarray((x, y, z)) - connectivity
            if is_inside(thickness.shape, tuple(e))
        ]:
            if flag_array[n] > 0:
                positive_sum += thickness[n]
                nb_point += 1

        if nb_point > 0:
            result_array[x, y, z] = positive_sum / nb_point

    # Return the clean thickness masked with the original volume and also
    # multiply with the resolution of the input to obtain value as mm.
    return result_array * mask * image_resolution


def largest_structure(data: np.ndarray) -> np.ndarray:
    """Keep the largest structure from each label.

    Args:
        data (np.ndarray): Binary input image.

    Returns:
        np.ndarray: Image with only biggest labels.
    """
    # Transforming input into a binary image & labelling it

    labeled = give_labels(data, background=0, return_num=False, connectivity=3)

    # Determining the number and size of unique structures in the image
    unique, counts = np.unique(labeled, return_counts=True)
    max_label = unique[np.where(counts == np.max(counts[1 : counts.shape[0]]))]

    # Setting everything except the largest structure to 0
    labeled[np.where(labeled != max_label)] = 0
    labeled[np.where(labeled != 0)] = 1

    return labeled


def diameter_transform(
    structure: np.ndarray, pix_dim: Sequence[float], threshold=0.5, return_steps=False
) -> Union[Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray], np.ndarray]:
    """Extract the diameter from a label image.

    Based on paper:

    New algorithms for Euclidean distance transformation on an n-dimensional
    digitized picture with applications," T. Saito and J. Toriwaki, Pattern
    Recognition 27 (1994) 1551-1565.

    Reverse Distance Transformation and Skeletons Based upon the Euclidean
    Metric For n-Dimensional Binary Pictures, T. Saito and J. Toriwaki, IEICE
    Trans. Inf. & Syst., Vol E77-D, No. 9, Sept. 1994

    Exact medial axis with euclidean distance, E. Remy and E. Thiel, Image and
    Vision Computing 23 (2005) 167-175.

    A new method for the model-independent assessment of thickness in
    three-dimensional images" T. Hildebrand and P. Rüesgsegger, J. of Microscopy,
    185 (1996) 67-75.

    http://www.optinav.com/Local_Thickness.htm

    Args:
        structure (np.ndarray): Binary input image.
        pix_dim (Sequence[float]): voxel size.
        threshold (float, optional): Threshold input image. Defaults to 0.5.
        return_steps (bool, optional): Return all the steps. Defaults to False.

    Raises:
        ValueError: If the pix_dim is not isotropic

    Returns:
        Union[Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray], np.ndarray]:
            Diameter or diameter and all the intermediate steps.
    """

    data = np.copy(structure)

    data[np.where(data > threshold)] = 1
    data[np.where(data <= threshold)] = 0

    # Making sure all pixel dimensions are close
    dim = pix_dim[0]
    if not np.all([np.isclose(dim, d) for d in pix_dim]):
        raise ValueError(
            "Your data must be isotropic to extract diameter. Now it is pix_dim=" + str(pix_dim)
        )

    edt = compute_distance_map(data)

    ridge_distance = compute_ridge_distance(edt)

    thickness = compute_thickness(ridge_distance)

    diameter_clean = compute_clean_thickness(data, thickness, pix_dim[0])

    # Returning Diameter Data
    if return_steps:
        return diameter_clean, edt, ridge_distance, thickness
    return diameter_clean


def special_end_points(coords1, coords2, value) -> bool:
    """Function that flags a slab point to an end points if the patch
    configuration is in that case:
        patch = [[0, 0, 0],
                 [0, 1, 0],
                 [0, 1, 1]]
     This is a 2D example but the patch is in 3D. We previously considered slab points
     all points with only 2 neighbors. The problem is that we did not take into account this
     special case which is an end points. By default this function take into account the neighbors
     patch (without the middle points) and say if the current patch is a slab points. The number
     of non_zero voxel in that patch must be equal to 2. Example of neighbors patch (2D for example)
     neighbors = [[0, 0, 0],    neighbors = [[1, 0, 0],      neighbors = [[0, 0, 0],
                  [0, 0, 0],                 [0, 0, 0],                   [1, 0, 1],
                  [0, 1, 1]]                 [0, 0, 1]]                   [0, 0, 0]]


    Args:
        coords1 (np.array): First non-zero coordinates of neighbors
        coords2 (np.array): Second non-zero coordinates of neighbors
        value (np.array): Non 1 index

    Returns:
        end_point (bool): Return is the current centered point is an end point

    """

    is_end_point = False
    if coords1.count(1) != 2:
        return is_end_point

    if value in coords1:
        if coords2[coords1.index(value)] == value:
            is_end_point = True

    return is_end_point


def analyze_skeleton(data: np.ndarray) -> np.ndarray:
    """Analyze the skeleton to find a tagged skeleton image with start, end, and slab.

    Args:
        data (np.ndarray): Input image.

    Returns:
        np.ndarray: tagged skeleton image
    """
    skl = data.astype("uint8")

    end_points = np.zeros(skl.shape, dtype=float)
    junction_points = np.zeros(skl.shape, dtype=float)
    slab_points = np.zeros(skl.shape, dtype=float)

    # Classifying centerline voxel based on neigborhood connectivity
    locations = (skl > 0).nonzero()
    for i, j, k in zip(*locations):
        neighbors = np.array(skl[i - 1 : i + 2, j - 1 : j + 2, k - 1 : k + 2])
        neighbors[1, 1, 1] = 0
        num_neighbors = np.count_nonzero(neighbors)

        if num_neighbors < 2:
            end_points[i, j, k] = 1
        elif num_neighbors > 2:
            junction_points[i, j, k] = 1
        elif num_neighbors == 2:
            slab_points[i, j, k] = 1
            coords1, coords2 = list(zip(*np.nonzero(neighbors)))
            is_end_points = []
            is_end_points.append(special_end_points(coords1, coords2, 0))
            is_end_points.append(special_end_points(coords1, coords2, 2))
            is_end_points.append(special_end_points(coords2, coords1, 0))
            is_end_points.append(special_end_points(coords2, coords1, 2))

            if np.any(is_end_points):
                end_points[i, j, k] = 1
                slab_points[i, j, k] = 0

    # Finding all junction neighborhoods and number of neighbors
    junction_adjacent_points = np.zeros(skl.shape, dtype=float)
    labelled_junction = give_labels(junction_points, connectivity=3)
    junction_points = np.zeros(junction_points.shape, dtype=float)

    neighborhoods, num_neighbors = np.unique(labelled_junction, return_counts=True)
    neighborhoods = neighborhoods[1:]
    num_neighbors = num_neighbors[1:]

    for neighborhood, num_neighbor in zip(neighborhoods, num_neighbors):
        # If the junction is composed of a single point, that point is a junction point
        if num_neighbor == 1:
            location = np.where(labelled_junction == neighborhood)
            coordinates = (location[0][0], location[1][0], location[2][0])
            junction_points[coordinates] = 1

        # If a junction is composed of more than a single point, then the center most point is a
        # junction point all other points are junction_adjacent_points
        else:
            # Determining Neighborhood Center of Mass Location
            points = np.zeros_like(junction_points)
            points[np.where(labelled_junction == neighborhood)] = 1
            center_point = center_of_mass(points)

            # Determining Neighborhood voxels distances to center of mass
            point_locations = np.where(points > 0)
            point_coordinates = np.transpose(point_locations).astype(np.float32)

            distances = dist.cdist(
                point_coordinates,
                np.array([list(center_point)]),
                metric="euclidean",
            )
            distances = distances.flatten()

            # Labelling Junction Point
            minimum = np.where(distances == np.min(distances))[0][0]
            coordinate = tuple(point_coordinates[minimum].astype(int).tolist())
            junction_points[coordinate] = 1

            # Labelling Other points as Junction Adjacent
            point_coordinates = np.delete(point_coordinates, minimum, axis=0)

            for vox in range(point_coordinates.shape[0]):
                coordinate = tuple(point_coordinates[vox].astype(int).tolist())
                junction_adjacent_points[coordinate] = 1

    tagged_skl = np.zeros_like(skl)
    tagged_skl[junction_points == 1] = 7
    tagged_skl[junction_adjacent_points == 1] = 6
    tagged_skl[end_points == 1] = 9
    tagged_skl[slab_points == 1] = 2

    return tagged_skl


def find_start_point(skeleton: np.ndarray, center: Sequence[float]) -> Sequence[int]:
    """Find the willis start point on the tagged skeleton.

    Args:
        skeleton (np.ndarray): Tagged skeleton image.
        center (Sequence[float]): Willis center of mass position.

    Returns:
        Sequence[int]: Coordinate of the willis start point
    """
    tagged_skl = np.copy(skeleton)

    # Finding Distance from Center of Mass of all End Points
    end_points = np.where(tagged_skl == 9)
    end_coordinates = np.transpose(end_points).astype(np.float32)

    distances = dist.cdist(end_coordinates, np.array([list(center)]))

    # Finding nearest point from willis center
    nearest_coordinates = end_coordinates[np.where(distances == np.min(distances))[0]]

    return nearest_coordinates[0].astype(int)


def find_skeleton_paths(
    start: Sequence[int], skeleton: np.ndarray, tag: int = 9
) -> Tuple[np.ndarray, Sequence[Tuple[int, int, int]]]:
    """Find the cost, path and indices in the tagged skeleton image.

    Args:
        start (Sequence[int]): Coordinate of willis start.
        skeleton (np.ndarray): Tagged skeleton image.
        tag (int, optional): Tag to use in the skeleton. Defaults to 9.

    Returns:
        Tuple[np.ndarray, Sequence[Tuple[int, int, int]]]: Cost and indices of the skeleton.
    """
    start_point = tuple(start)
    tagged_skl = np.ascontiguousarray(skeleton, dtype=int)

    # Finding ends and their coordinates
    ends = np.where(tagged_skl == tag)
    coords = list(zip(*ends))
    if start_point in coords:
        coords.remove(start_point)

    weights = np.where(tagged_skl != 0, 1, np.sum(tagged_skl))

    path_indices = []
    cost_stack = []

    for coord in coords:

        path, cost = route_through_array(
            weights,
            start_point,
            coord,
            fully_connected=True,
            geometric=True,
        )

        path_indices.append(path)
        cost_stack.append(cost)

    cost_array = np.array(cost_stack)

    return cost_array, path_indices


class Graph:
    def __init__(self, tagged_skl: np.array):
        self.skl = np.ascontiguousarray(tagged_skl, dtype=int)

    def create_adjacency_lists(self) -> Tuple[List, defaultdict]:
        """Create an adjacency list of a tagged skeleton.
        Meaning that the key is a voxel coordinates and its adjacencies
        are the connected voxels coordinates

        Returns:
            coordinates (List): List of coordinates of the tagged skeleton
            adjacency_list (defaultdict): Dictionnary containing voxel connectivity

        """
        adjacency_lists = collections.defaultdict(set)
        coordinates = (self.skl > 0).nonzero()
        coordinates = list(zip(*coordinates))
        for ind, coord in enumerate(coordinates):
            for other_coord in coordinates[ind:]:
                if (
                    abs((coord[0] - other_coord[0])) <= 1
                    and abs((coord[1] - other_coord[1])) <= 1
                    and abs((coord[2] - other_coord[2])) <= 1
                ):
                    adjacency_lists[ind].add(coordinates.index(other_coord))
                    adjacency_lists[coordinates.index(other_coord)].add(ind)

        for k, v in adjacency_lists.items():
            if k in v:
                v.remove(k)
            adjacency_lists[k] = v

        return coordinates, adjacency_lists

    def longest_path_g(
        self,
        start_point: Tuple[int, int, int],
        coordinates: List,
        adjacency_lists: defaultdict,
        tag: int = 9,
    ) -> Tuple[np.array, List]:
        """Create a graph from an adjacency dictionnary and return the longest path in the graph

        Args:
            start_point (Tuple[int, int, int]): Start coordinates
            coordinates (List): List of coordinates of the tagged skeleton
            adjacency_lists (defaultdict): Adjacency list dictionnary containing voxel connectivity
            tag (int): Tag corresponding to a end point in the tagged skeleton

        Returns:
            longest_path_array (np.array): Longest path array
            path2coord (List): List of coordinates in the longest path.

        """
        G = nx.Graph()

        ends = np.where(self.skl == tag)
        coords_end = list(zip(*ends))
        if start_point in coords_end:
            coords_end.remove(start_point)

        start_idx = coordinates.index(start_point)

        len_longest_path = 0
        longest_path = None
        for end_point in coords_end:
            end_idx = coordinates.index(end_point)
            for k, v in adjacency_lists.items():
                for value in v:
                    G.add_node(k)
                    G.add_edge(k, value)

            path = max(nx.all_simple_paths(G, start_idx, end_idx), key=lambda x: len(x))
            if len(path) > len_longest_path:
                len_longest_path = len(path)
                longest_path = path

        path2coord = []
        for idx in longest_path:
            path2coord.append(coordinates[idx])

        longest_path_array = np.zeros_like(self.skl)

        for coord in path2coord:
            longest_path_array[coord] = 1

        return longest_path_array, path2coord


def find_true_skeleton(
    indices: Sequence[Tuple[int, int, int]], skeleton_shape: Tuple[int, int, int]
) -> np.ndarray:
    """Extract the true skeleton images from the indices.

    Args:
        indices (Sequence[Tuple[int, int, int]]): Indices of the skeleton.
        skeleton_shape (Tuple[int, int, int]): Shape of the skeleton image.

    Returns:
        np.ndarray: Skeleton mask.
    """
    true_skl = np.zeros(skeleton_shape)

    true_skl[list(zip(*it.chain(*indices)))] = 1

    return true_skl


def generate_frequency_path(
    indices: Sequence[Tuple[int, int, int]], skeleton_shape: Tuple[int, int, int]
) -> np.array:
    """Generate image of the frequency of each artery segment

    Args:
        indices (Sequence[Tuple[int, int, int]]): List containing tuples position of each path
        skeleton_shape (Tuple[int, int, int]): Shape of the skeleton image

    Returns:
        frequency_path (np.array): Frequency path image
    """
    frequency_path = np.zeros(skeleton_shape)

    for indice in indices:
        tmp = np.zeros(skeleton_shape)
        zip_indice = list(zip(*indice))
        tmp[zip_indice] = 1
        frequency_path += tmp

    return frequency_path


def find_longest_path(
    indices: Sequence[Tuple[int, int, int]],
    skeleton_shape: Tuple[int, int, int],
    distance: np.array,
) -> Union[Tuple[np.ndarray, np.ndarray], np.ndarray]:
    """Find the longuest path given the cost and path images.

    Args:
        costs (np.ndarray): Cost of each voxel.
        indices (Sequence[Tuple[int, int, int]]): Paths of the artery.
        skeleton_shape (skeleton_shape): Shape of the skeleton array

    Returns:
        Union[Tuple[np.ndarray, np.ndarray], np.ndarray]: Return longuest path mask and most.
    """
    max_coord = np.unravel_index(distance.argmax(), distance.shape)
    longest_path_indices = [path for path in indices if max_coord in path][0]
    longest_path = np.zeros(skeleton_shape, dtype=np.float64)

    for position in longest_path_indices:
        longest_path[position] = 1

    return longest_path


def compute_vascular_distance(
    data: np.ndarray,
    indices: Sequence[Tuple[int, int, int]],
    path_frequency: np.ndarray,
    dims: Sequence[float],
) -> Tuple[np.ndarray, np.ndarray, np.ndarray, float, float]:
    """Compute several metrics for an artery mask, distance, tortuosity, angulation,
    corrected_length and average tortuosity.

    Args:
        data (np.ndarray): Mask of the artery.
        indices (np.ndarray): indices of the skeleton.
        path_frequency (np.ndarray): array of the summation of the frequency of paths.
        dims (Sequence[float]): Voxel size.

    Returns:
        Tuple[np.ndarray, np.ndarray, np.ndarray, float, float]: distance, tortuosity, angulation,
            corrected_length and average tortuosity.
    """
    num_paths = len(indices)
    ordered_distances = []
    ordered_geo_distances = []
    ordered_angles = []

    for order in indices:
        start = np.zeros((1, 3))
        start[0] = order[0]

        ordered_distance = [0.0]
        ordered_geo_distance = [0.0]
        ordered_angle = [0.0]

        for j in range(1, len(order)):
            # Computing vascular wise distance
            xa = np.zeros((1, 3))
            xb = np.zeros((1, 3))
            xa[0] = order[j - 1]
            xb[0] = order[j]
            vasc_dist = dist.cdist(xa, xb, metric="euclidean")
            vasc_dist.reshape(-1)
            ordered_distance.append(vasc_dist[0][0] + ordered_distance[j - 1])

            # Computing geometric Distance from start point
            geo_dist = dist.cdist(start, xb, metric="euclidean")
            geo_dist.reshape(-1)
            ordered_geo_distance.append(geo_dist[0][0])

            # Computing Angulation
            if j == len(order) - 1:
                ordered_angle.append(0.0)
            else:
                oa = np.array(list(order[j - 1]))
                ob = np.array(list(order[j]))
                oc = np.array(list(order[j + 1]))
                v1 = ob - oa
                v1 = v1 / np.linalg.norm(v1)
                v2 = oc - ob
                v2 = v2 / np.linalg.norm(v2)
                dot = np.dot(v1, v2)
                angle = np.arccos(dot) * 180 / math.pi

                if np.isnan(angle):
                    angle = 0

                ordered_angle.append(angle)
        ordered_distances.append(ordered_distance)
        ordered_geo_distances.append(ordered_geo_distance)
        ordered_angles.append(ordered_angle)

    distance_dict = {}
    geo_dist_dict = {}
    angle_dict = {}

    for orders, distances, geo_distances, angles in zip(
        indices, ordered_distances, ordered_geo_distances, ordered_angles
    ):

        for order, distance, geo_distance, angle in zip(orders, distances, geo_distances, angles):
            # VascularDistance
            d_list = distance_dict.get(order, [])
            d_list.append(distance)
            distance_dict[order] = d_list
            # GeometricDistance
            g_list = geo_dist_dict.get(order, [])
            g_list.append(geo_distance)
            geo_dist_dict[order] = g_list
            # Angulation
            a_list = angle_dict.get(order, [])
            a_list.append(angle)
            angle_dict[order] = a_list

    distance_array = np.zeros_like(data, np.float64)
    for key, value in distance_dict.items():
        distance_array[key] = np.mean(value)

    geo_array = np.zeros_like(data, np.float64)
    for key, value in geo_dist_dict.items():
        geo_array[key] = np.mean(value)

    tortuosity_array = distance_array / geo_array
    tortuosity_array = np.nan_to_num(tortuosity_array)

    angle_array = np.zeros_like(data, np.float64)
    for key, value in angle_dict.items():
        angle_array[key] = sum(value) / len(value)

    total_length = sum([ordered_distances[i][-1] for i in range(num_paths)])
    corrected_length = total_length
    for i in np.unique(path_frequency)[1:]:
        segments = (path_frequency == i).astype(float)
        segments = give_labels(segments, connectivity=3)
        for seg in np.unique(segments)[1:]:
            end_dist = distance_array[segments == seg].max()
            start_dist = distance_array[segments == seg].min()
            difference = (end_dist - start_dist) * (i - 1)
            corrected_length -= difference

    average_tortuosity = (
        sum([ordered_distances[i][-1] * tortuosity_array[indices[i][-1]] for i in range(num_paths)])
        / total_length
    )

    distance_array *= np.mean(dims)
    corrected_length *= np.mean(dims)

    return (
        distance_array,
        tortuosity_array,
        angle_array,
        corrected_length,
        average_tortuosity,
    )


def generate_orders_map(
    frequency: np.array,
    main_trunk: np.array,
    indices: Sequence[Tuple[int, int, int]],
) -> np.array:
    """Function that generate orders artery map

    Args:
        frequency (np.array): Frequency map
        main_trunk (np.array): Main trunk
        indices (Sequence[Tuple[int, int, int]]): Path indices in the artery

    Returns:
        order_path_all (np.array): Orders map

    """
    full_image_shape = list(frequency.shape)
    full_image_shape.append(len(indices))
    full_argmin = np.zeros(tuple(full_image_shape))
    frequency_mod = frequency.copy()

    unique_main = np.unique(frequency_mod[main_trunk > 0])
    frequency_mod[main_trunk > 0] = np.amax(unique_main)

    for i, indice in enumerate(tqdm(indices)):
        image = np.zeros_like(frequency)
        for position in indice:
            image[position] = 1

        current_frequency = image * frequency_mod
        order_path = np.zeros_like(current_frequency)
        for j, freq in enumerate(np.unique(current_frequency)[::-1][:-1]):
            order_path[current_frequency == freq] = j + 1

        full_argmin[..., i] = order_path

    full_argmin[full_argmin == 0] = np.finfo("f").max
    order_path_all = np.amin(full_argmin, axis=-1)
    order_path_all[order_path_all == np.finfo("f").max] = 0

    return order_path_all


def find_main_trunk(
    frequency: np.array, start: Sequence[int], distance: np.array, main_trunk_stop: float = 0
) -> np.array:
    """Find main trunk of the artery.

    Args:
        frequency (np.array): Frequency path image.
        start (Sequence[int]): Start point of the artery.
        distance (np.array): Distance map
        main_trunk_stop (float): Value between 0 and 1

    Returns:
        main_trunk (np.array): Main trunk of the artery.
    """

    # Iterative Longest path finding for path's most travelled
    main_trunk = np.zeros(frequency.shape, dtype=float)
    unique = np.unique(frequency).astype(int)[1:].tolist()[::-1]
    first_order_val = None

    if not 0 <= main_trunk_stop <= 1:
        raise ValueError("Main trunk stop must be between 0 and 1. Got {}".format(main_trunk_stop))

    for segment in unique:
        logging.info("   +++: Starting Segment %f" % (segment,))

        skl = np.zeros(frequency.shape, dtype=float)
        skl[np.where(frequency == segment)] = 1

        skl[tuple(start)] = 1
        skl_labels = give_labels(skl, connectivity=3)

        skl = np.zeros(frequency.shape, dtype=float)
        skl[np.where(skl_labels == skl_labels[tuple(start)])] = 1

        # Checking if this segment is empty (aka, the start point isn't connected to this path frequency)
        if not np.count_nonzero(skl) == 1:
            if first_order_val is not None:
                if segment < main_trunk_stop * first_order_val:
                    break
            tagged_skl = analyze_skeleton(data=skl)

            costs, orders = find_skeleton_paths(tag=9, skeleton=tagged_skl, start=start)

            distance_current = (tagged_skl > 0).astype(float) * distance
            longest = find_longest_path(
                indices=orders, skeleton_shape=skl.shape, distance=distance_current
            )

            main_trunk += longest

            # Updating start point for next iteration
            analysed_longest = longest * tagged_skl
            ends = np.where(analysed_longest == 9)

            ends_coords = np.transpose(ends).tolist()

            if start.tolist() in ends_coords:
                ends_coords.remove(start.tolist())

            start = np.array(ends_coords[0])
            first_order_val = segment
            logging.info(f"   +++: New Start Point: {start}")

        else:
            logging.info("   +++: Segment not connected to main trunk thus far...")

    return main_trunk


def reduce_wb_analysis(
    orders: np.array,
    distance: np.array,
    longest_path: np.array,
    indices: Sequence[Tuple[int, int, int]],
    diameter: np.array,
) -> collections.OrderedDict:

    average_distance = []
    for indice in indices:
        image = np.zeros_like(orders)
        for position in indice:
            image[position] = 1
        image_distance = image * distance
        average_distance.append(image_distance.max())

    average_distance = np.mean(average_distance)

    longest_path_distance = longest_path * distance
    longest_distance = np.amax(longest_path_distance)

    path_fraction = average_distance / longest_distance

    unique_orders = np.unique(orders)[1:]
    n_segment = []
    diameter_segment_mean = []
    diameter_segment_std = []
    for order in unique_orders:
        current_order = np.zeros(orders.shape, dtype=np.float64)
        current_order[orders == order] = 1
        current_order_diameter = current_order * diameter
        current_order_diameter = current_order_diameter[
            np.where(current_order_diameter != 0)
        ].flatten()
        diameter_segment_mean.append(current_order_diameter.mean())
        diameter_segment_std.append(current_order_diameter.std())
        current_order_label = give_labels(current_order, connectivity=3)
        unique_label = np.unique(current_order_label)[1:]
        n_segment.append(len(unique_label))

    statistics = collections.OrderedDict((("Path fraction", path_fraction),))

    for i, (n, mean, std) in enumerate(zip(n_segment, diameter_segment_mean, diameter_segment_std)):
        statistics[f"N Segment Order {i + 1}"] = n
        statistics[f"Mean Diam Order {i + 1}"] = mean
        statistics[f"STD Diam Order {i + 1}"] = std

    return statistics


def compute_distance_wise_diameter(
    artery: np.array, distance: np.array, diameter: np.array, step: float = 1.0
) -> pd.DataFrame:
    """Compute distance wise diameter.

    Args:
        artery (np.array): Artery image.
        distance (np.array): Distance image.
        diameter (np.array): Diameter image.
        step (float): Step in mm of the distance in the Dataframe output.

    Returns:
        df (pd.DataFrame): Dataframe with distance wise diameter table.
    """

    distance = np.nan_to_num(distance)
    diameter = np.nan_to_num(diameter)
    artery = np.nan_to_num(artery)

    # Creating Distance_Diameter Dictionnary ( dict = {distance: diameter, etc.})
    indexes = np.where(artery != 0)
    dist_diam_dict = {distance[index]: diameter[index] for index in zip(*indexes)}

    bins = np.arange(0, max(dist_diam_dict.keys()), step)
    distances = np.array(list(dist_diam_dict.keys()))
    inds = np.digitize(distances, bins)

    # Reformatting dictionnary into bin format ( dict = {bin: [ diameter1, diameter2, etc. ], etc.})
    lut = dict(zip(distances, inds))
    dist_diam_bin_dict = {}

    for key, value in lut.items():
        lst = dist_diam_bin_dict.get(value, [])
        lst.append(dist_diam_dict[key])
        dist_diam_bin_dict[value] = lst

    ordered_dict = collections.OrderedDict(sorted(dist_diam_bin_dict.items()))

    df_data = {
        "Distance": [],
        "Mean": [],
        "Median": [],
        "STDEV": [],
        "SEM": [],
        "95%_low": [],
        "95%_high": [],
        "N": [],
    }

    for key, data in ordered_dict.items():
        data = np.asarray(data)
        mean = np.mean(data)
        sem = stats.sem(data)
        N = data.size
        confidence = stats.t.interval(0.95, N - 1, loc=mean, scale=sem)
        df_data["Distance"].append(key * step)
        df_data["Mean"].append(mean)
        df_data["Median"].append(np.median(data))
        df_data["STDEV"].append(np.std(data))
        df_data["SEM"].append(sem)
        df_data["95%_low"].append(confidence[0])
        df_data["95%_high"].append(confidence[1])
        df_data["N"].append(N)

    df = pd.DataFrame.from_dict(df_data)

    return df
