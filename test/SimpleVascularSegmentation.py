#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script that extract the Circle of Willis from a Time-of-flight images.
"""

import argparse
import logging
import os
from os.path import join, exists
import sys

import nibabel as nib
import numpy as np

from vessel_segmentation.prediction import neuronal_network_combination
from vessel_segmentation.tof_master import (
    autobox_image,
    create_willis_cube,
    hysteresis_thresholding_cube,
    mask_image,
    resample,
    apply_transform,
)


def parse_arguments() -> argparse.Namespace:
    """Simple CommandLine argument parsing function making use of the argsparse module.

    Returns:
        argparse.Namespace: command line arguments.
    """
    parser = argparse.ArgumentParser(
        prog="Vessel Segmentation",
        description="Vascular Medical Image Automatic Analysis. " "Optimize for CW analysis.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "TOF",
        help="ToF MRA image [ 3D image | .nii/ .nii.gz ] or directory holding ToF Images for a "
        "number of subjects images must contain *TOF/CTA*.nii.gz and can all be in the "
        "input directory or in its subdirectories.",
    )
    parser.add_argument(
        "output",
        help="Defines the output folder. Notice if you use this parameter keep in mind that some "
        "nifti header could differ with the afni processing.",
    )
    parser.add_argument(
        "-f",
        "--overwrite",
        help="Force overwriting of the output files.",
        action="store_true",
    )
    parser.add_argument(
        "--device",
        help="Device type, choices=[ cpu, cuda ].",
        choices=["cpu", "cuda"],
        default=None,
    )
    parser.add_argument(
        "-d",
        "--down",
        help="If flagged, registration is done to a down sampled MNI template to save time.",
        action="store_const",
        const="_down",
        default="",
    )
    parser.add_argument(
        "-c",
        "--cube",
        help="Cube containing the circle of willis [ 3D image | .nii/ .nii.gz ].",
        default=None,
    )
    parser.add_argument(
        "-m",
        "--mask",
        help="Brain mask [ 3D image | .nii/ .nii.gz ].",
        default=None,
    )
    parser.add_argument(
        "--MNITransform",
        help="If used the input file is used as the *0GenericAffine.mat file for an ANTs of MNI "
        "Space to Native space.",
    )
    parser.add_argument(
        "--InvertTransform",
        help="If flagged the inverse of the input transform is used for MNI registration to "
        "Native Space.",
        action="store_true",
    )
    parser.add_argument(
        "-t",
        "--template_path",
        help="Path Leading to AVG_TOF_MNI_SS.nii.gz/willis_sphere.nii.gz/SSS.nii.gz or their "
        "*_down.nii.gz counter parts.",
        required=True,
    )
    parser.add_argument(
        "-mp",
        "--models_path",
        help="Path Leading to trained Neural Network Model.",
    )
    parser.add_argument(
        "-vs", "--version", help="Version used", default="PCASCA", choices=["OLD", "PCASCA"]
    )
    parser.add_argument(
        "-a", "--attention", help="If flag, an attention model is used", action="store_true"
    )

    parser.add_argument(
        "-s", "--save_extras", help="If flag, cube markers, SSS and mask_cube_sss are saved", action="store_true"
    )
    parser.add_argument("-p", "--prefix", help="output file prefix.")
    parser.add_argument(
        "-v",
        "--verbose",
        help="If flagged progress is reported, use -vv for more information.",
        action="count",
    )
    args = parser.parse_args()

    logging.info(args)

    return args


def save_cube_markers(args, prefix, cube, markers, tof, nii_attributes):
    """

    Args:
        cube:
        markers:
        tof:

    Returns:

    """

    (sag, coro, axial) = np.nonzero(cube)
    sag_min = sag.min()
    sag_max = sag.max()
    coro_min = coro.min()
    coro_max = coro.max()
    axial_min = axial.min()
    axial_max = axial.max()

    markers_cube = markers[sag_min:sag_max, coro_min:coro_max, axial_min:axial_max]
    tof_cube = tof[sag_min:sag_max, coro_min:coro_max, axial_min:axial_max]

    markers_cube_file = join(args.output, f"{prefix}_markers_cube.nii.gz")
    nib.Nifti1Image(markers_cube, nii_attributes.affine, nii_attributes.header).to_filename(
        markers_cube_file
    )

    tof_cube_file = join(args.output, f"{prefix}_tof_cube.nii.gz")
    nib.Nifti1Image(tof_cube, nii_attributes.affine, nii_attributes.header).to_filename(
        tof_cube_file
    )


def full_pipeline(args: argparse.Namespace, prefix: str):
    """Compute the preprocessing, prediction, and metrics.

    Args:
        args (argparse.Namespace): Command line arguments.
        prefix (str): File name prefix.
    """

    resolution_nn = (0.625, 0.625, 0.625)

    template_path = join(args.template_path, f"AVG_TOF_MNI_SS{args.down}.nii.gz")
    template_sphere_path = join(args.template_path, f"willis_sphere{args.down}.nii.gz")
    tof_raw: nib.Nifti1Image = nib.load(args.TOF)
    logging.info(f"Original orientation: {nib.aff2axcodes(tof_raw.affine)}")

    tof = nib.as_closest_canonical(tof_raw)
    logging.info(f"Transformed orientation: {nib.aff2axcodes(tof.affine)}")

    reorient_file = join(args.output, f"{prefix}_reorient.nii.gz")
    tof.to_filename(reorient_file)

    logging.info("1. ===> Preprocessing <===")
    logging.info("   1.1 ++++ : Resampling to Neural Network Resolution")
    resample_file = join(args.output, f"{prefix}_resampled.nii.gz")
    nii_attributes = resample(reorient_file, resample_file, resolution=resolution_nn)
    resampled_array = nii_attributes.get_fdata("unchanged")

    logging.info("2.  ===> Willis Labelling <===")
    logging.info("   2.1 ++++ : Creating a Brain Mask of NN Resolution")

    if args.mask:
        logging.info(f"    Used parse mask: {args.mask}")
        mask_file = join(args.output, f"{prefix}_mask.nii.gz")
        nii_attributes_mask = resample(
            args.mask, mask_file, master=resample_file, resample_mode="NN"
        )
        mask_array = nib.as_closest_canonical(nii_attributes_mask).get_fdata()
        brain_mask_array = mask_array * resampled_array
        brain_mask_file = join(args.output, f"{prefix}_SS_Input.nii.gz")
        nib.Nifti1Image(brain_mask_array, nii_attributes.affine, nii_attributes.header).to_filename(
            brain_mask_file
        )
        nib.Nifti1Image(mask_array, nii_attributes.affine, nii_attributes.header).to_filename(
            mask_file
        )
    else:
        mask_file = join(args.output, f"{prefix}_mask.nii.gz")
        brain_mask_file = join(args.output, f"{prefix}_SS_RegistrationImage.nii.gz")
        mask_image(resample_file, mask_file, brain_mask_file)

    logging.info("   2.2 ++++ : Finding Circle of Willis masking cube")

    cube_file = join(args.output, f"{prefix}_cube.nii.gz")
    if args.cube is None:
        sphere, cube = create_willis_cube(
            brain_mask_file,
            args.output,
            template_path,
            template_sphere_path,
            transform=args.MNITransform,
            invert=args.InvertTransform,
        )
        nib.Nifti1Image(sphere, nii_attributes.affine, nii_attributes.header).to_filename(
            join(args.output, f"{prefix}_sphere.nii.gz")
        )
        nib.Nifti1Image(cube, nii_attributes.affine, nii_attributes.header).to_filename(cube_file)
    else:
        cube = resample(args.cube, cube_file, master=resample_file, resample_mode="NN").get_fdata(
            "unchanged"
        )

    logging.info("   2.3 ++++ : Predicting Circle of willis markers")

    markers_file = join(args.output, f"{prefix}_markers.nii.gz")

    voxel_volume: float = np.prod(resolution_nn)

    binary_image = hysteresis_thresholding_cube(resampled_array, cube)

    nib.Nifti1Image(binary_image, nii_attributes.affine, nii_attributes.header).to_filename(
        join(args.output, f"{prefix}_bin_vessels.nii.gz")
    )

    markers = neuronal_network_combination(
        resampled_array,
        args,
        prefix,
        cube,
        binary_image,
        voxel_volume,
        nii_attributes,
        resample_file,
        markers_file,
    )

    if args.save_extras:
        save_cube_markers(args, prefix, cube, markers, resampled_array, nii_attributes)

        ss_mask = join(args.template_path, f"SSS_masked{args.down}.nii.gz")
        ss_mask_reg = apply_transform(
            ss_mask,
            resample_file,
            join(args.output, "WILLIS_ANTS0GenericAffine.mat"),
            join(args.output, "SSS_masked_reg.nii.gz"),
        )

        mask_array = nib.load(mask_file).get_fdata()
        mask_cube = ((np.logical_or(mask_array, cube).astype(float) - ss_mask_reg) > 0).astype(
            np.uint8
        )
        nib.Nifti1Image(mask_cube, nii_attributes.affine, nii_attributes.header).to_filename(
            join(args.output, f"{prefix}_mask_cube_sss.nii.gz")
        )


def main():
    args = parse_arguments()

    if exists(args.output):
        if not args.overwrite:
            print(f"Outputs directory {args.output} exists. Use -f to for overwriting.")
            sys.exit(1)
    else:
        os.makedirs(args.output)

    if args.verbose:
        # TODO: I don't know why but the logging is instanciated before and we can't change it
        # with logging.baseConfig(...).
        fmt = logging.Formatter("%(asctime)-15s %(message)s", None, "%")
        for h in logging.root.handlers:
            h.setFormatter(fmt)
        logging.root.setLevel(logging.INFO if args.verbose == 1 else logging.DEBUG)

    logging.info("~ =============> SimpleVascularSegmentation <============= ~")
    logging.info(f"TOF: {args.TOF}")
    logging.info(f"Output: {args.output}")
    logging.info(f"Device: {args.device}")
    logging.info(f"Prefix: {args.prefix}")
    logging.info(f"Cube: {args.cube}")
    logging.info(f"Mask: {args.mask}")
    logging.info(f"Down: {args.down}")
    logging.info(f"Models path: {args.models_path}")
    logging.info(f"Attention: {args.attention}")
    logging.info(f"TemplatePath: {args.template_path}")
    logging.info(f"MRITransform: {args.MNITransform}")
    logging.info(f"InvertTransform: {args.InvertTransform}")
    logging.info(f"Save Extras: {args.save_extras}")
    logging.info(f"Verbose: {args.verbose}")

    assert len(os.listdir(args.models_path)) > 0, "Please provide a model in your models path"

    prefix = args.prefix
    if prefix is None:
        file_path = os.path.basename(args.TOF)
        prefix = file_path.replace(".gz", "").replace(".nii", "")

    full_pipeline(args, prefix)


if __name__ == "__main__":
    main()
