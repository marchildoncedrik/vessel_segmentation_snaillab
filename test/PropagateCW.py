#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Script that extract the Circle of Willis from a Time-of-flight images.
"""

import argparse
import logging
import os
from os.path import join, exists
import sys
from typing import Tuple

import nibabel as nib
import numpy as np

from vessel_segmentation.arterial_vessel import (
    diameter_transform,
)

from vessel_segmentation.tof_master import (
    centerline_transform,
    edt_transform,
    extend_markers,
    correct_watershed,
    watershed_segment,
)


def parse_arguments() -> argparse.Namespace:
    """Simple CommandLine argument parsing function making use of the argsparse module.

    Returns:
        argparse.Namespace: command line arguments.
    """

    parser = argparse.ArgumentParser(
        prog="Vessel Segmentation",
        description="Vascular Medical Image Automatic Analysis. " "Optimize for CW analysis.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "CW",
        help="CW markers [ 3D image | .nii/ .nii.gz ]",
    )
    parser.add_argument(
        "ARTMASK",
        help="Defines the output folder. Notice if you use this parameter keep in mind that some "
        "nifti header could differ with the afni processing.",
    )
    parser.add_argument(
        "output",
        help="Defines the output folder. Notice if you use this parameter keep in mind that some "
        "nifti header could differ with the afni processing.",
    )
    parser.add_argument(
        "-c", "--connectivity", help="Connectivity", default=1, type=int, choices=[1, 2, 3]
    )
    parser.add_argument(
        "-f",
        "--overwrite",
        help="Force overwriting of the output files.",
        action="store_true",
    )
    parser.add_argument("-p", "--prefix", help="output file prefix.")

    parser.add_argument(
        "-v",
        "--verbose",
        help="If flagged progress is reported, use -vv for more information.",
        action="count",
    )
    args = parser.parse_args()

    logging.info(args)

    return args


def extract_centerline_and_diameter(
    tof: nib.Nifti1Image,
    markers_bin: np.ndarray,
    prefix: str,
    output: str,
    brain_vasculature: str = "CircleOfWillis",
) -> Tuple[np.ndarray, np.ndarray]:
    """Extract center line and diameter from markers mask.

    Args:
        tof (nib.Nifti1Image): Time of flight image.
        markers_bin (np.ndarray): Markers result of the segmentation.
        prefix (str): File name prefix.
        output (str): Output directory.
        brain_vasculature (str, optional): Defaults to "CircleOfWillis".

    Returns:
        Tuple[np.ndarray, np.ndarray]: center line and diameter images.
    """
    logging.info("4. ===> Arterial Centerline & Diameter <===")
    logging.info("   4.1 ++++ : Extracting Centerline")
    center_line = centerline_transform(markers_bin)
    nib.Nifti1Image(center_line, tof.affine, tof.header).to_filename(
        join(output, f"{prefix}_{brain_vasculature}_centerline.nii.gz")
    )

    logging.info("   4.2 ++++ : Extracting Diameter")
    diameter = diameter_transform(markers_bin, tof.header.get_zooms())

    nib.Nifti1Image(diameter, tof.affine, tof.header).to_filename(
        join(output, f"{prefix}_{brain_vasculature}_diameter.nii.gz")
    )

    return center_line, diameter


def vessels_flood_filling(
    markers_data: np.array,
    mask: np.array,
    affine: np.array,
    header: nib.Nifti1Header,
    output: str,
    prefix: str,
    connectivity: int = 1,
) -> np.array:
    """Watershed markers in the binary vessel image

    Args:
        markers_data (np.array): Circle of Willis.
        mask (np.array): Brain mask.
        affine (np.array): Affine matrix.
        header (nib.Nifti1Header): Header.
        output (str): Output directory.
        prefix (str): File name prefix.
        connectivity (int): Connectivity of the FF

    Returns:
        labels (np.array): Watershed image.
    """

    labels_file = join(output, f"{prefix}_labels_c_{connectivity}.nii.gz")

    if connectivity == 1:
        connectivity = np.array(
            [
                [[0, 0, 0], [0, 1, 0], [0, 0, 0]],
                [[0, 1, 0], [1, 1, 1], [0, 1, 0]],
                [[0, 0, 0], [0, 1, 0], [0, 0, 0]],
            ]
        )
    elif connectivity == 2:
        connectivity = np.array(
            [
                [[0, 1, 0], [0, 1, 0], [0, 1, 0]],
                [[1, 1, 1], [1, 1, 1], [1, 1, 1]],
                [[0, 1, 0], [0, 1, 0], [0, 1, 0]],
            ]
        )
    elif connectivity == 3:
        connectivity = np.array(
            [
                [[1, 1, 1], [1, 1, 1], [1, 1, 1]],
                [[1, 1, 1], [1, 1, 1], [1, 1, 1]],
                [[1, 1, 1], [1, 1, 1], [1, 1, 1]],
            ]
        )

    labels = extend_markers(mask, markers_data, connectivity=connectivity)
    labels = correct_watershed(markers_data, labels)
    nib.Nifti1Image(labels, affine, header).to_filename(labels_file)

    return labels


def main():
    args = parse_arguments()

    if exists(args.output):
        if not args.overwrite:
            print(f"Outputs directory {args.output} exists. Use -f to for overwriting.")
            sys.exit(1)
    else:
        os.makedirs(args.output)

    if args.verbose:
        # TODO: I don't know why but the logging is instanciated before and we can't change it
        # with logging.baseConfig(...).
        fmt = logging.Formatter("%(asctime)-15s %(message)s", None, "%")
        for h in logging.root.handlers:
            h.setFormatter(fmt)
        logging.root.setLevel(logging.INFO if args.verbose == 1 else logging.DEBUG)

    logging.info("~ =============> PropagateCW <============= ~")
    logging.info(f"CW: {args.CW}")
    logging.info(f"ARTMASK: {args.ARTMASK}")
    logging.info(f"Output: {args.output}")
    logging.info(f"Connectivity: {args.connectivity}")
    logging.info(f"Prefix: {args.prefix}")
    logging.info(f"Overwrite: {args.overwrite}")
    logging.info(f"Verbose: {args.verbose}")

    prefix = args.prefix
    if prefix is None:
        file_path = os.path.basename(args.ARTMASK)
        prefix = file_path.replace(".gz", "").replace(".nii", "")

    cw = nib.load(args.CW)
    cw_array = cw.get_fdata()
    affine = cw.affine
    header = cw.header

    mask = nib.load(args.ARTMASK)
    mask_array = mask.get_fdata()
    assert cw_array.shape == mask_array.shape, "ARTMASK and CWMASK do not have the same shape"

    labels = vessels_flood_filling(
        cw_array,
        mask_array,
        affine,
        header,
        output=args.output,
        prefix=prefix,
        connectivity=args.connectivity,
    )

    _, _ = extract_centerline_and_diameter(
        cw,
        labels.astype(bool).astype(float),
        prefix,
        args.output,
        brain_vasculature="CerebralArteries",
    )


if __name__ == "__main__":
    main()
