#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import os
import sys
from collections import OrderedDict
import logging
from os.path import join, basename, exists
from typing import Sequence

import nibabel as nib
from nibabel.streamlines.tractogram import Tractogram
from nibabel.streamlines.trk import TrkFile
import numpy as np
from skimage.morphology import skeletonize_3d

from vessel_segmentation import ARTERIAL_MARKER_LIST
from vessel_segmentation.arterial_vessel import (
    analyze_skeleton,
    diameter_transform,
    find_skeleton_paths,
    find_start_point,
    largest_structure,
)
from vessel_segmentation.Timer import Timer
from vessel_segmentation.tof_master import find_willis_center


def parse_arguments() -> argparse.Namespace:
    """Simple CommandLine argument parsing function making use of the argsparse module.

    Returns:
        argparse.Namespace: parsed arguments object args.
    """
    parser = argparse.ArgumentParser(description="Return tracks of the vascular system")
    parser.add_argument("output", help="Output directory.")
    parser.add_argument("labels", help="Segmentation labels. [ 3D image | .nii/ .nii.gz ]")
    parser.add_argument("markers", help="Circle of willis markers. [ 3D image | .nii/ .nii.gz ]")

    parser.add_argument(
        "-v",
        "--verbose",
        help="If flagged progress is reported, use -vv for more informations.",
        action="count",
    )

    args = parser.parse_args()

    return args


def compute_arterial_metrics_full(
    markers_data: np.ndarray,
    vessels_labeled: np.ndarray,
    center_line: np.ndarray,
    center: Sequence[float],
    affine: np.ndarray,
    header: nib.Nifti1Header,
    args: argparse.Namespace,
    prefix: str,
):
    """Compute arterial metrics for each artery in the brain.

    Args:
        markers_data (np.ndarray): Markers segmentation.
        vessels_labeled (np.array): Image of the vessels with a tag.
        center_line (np.ndarray): Center line image of the vessels labeled.
        center (Sequence[float]): Center of mass of the markers.
        affine (np.ndarray): Reference affine.
        header (nib.Nifti1Header): Reference header.
        prefix (str): Use as prefix of filename.

    """

    logging.info("3. ===> Computing Arterial Metrics <===")

    global_indices = []
    for label in np.unique(vessels_labeled)[1:]:
        if label > len(ARTERIAL_MARKER_LIST):
            logging.warning(f"Label {label} isn't an identified artery!")
            continue

        artery_prefix = ARTERIAL_MARKER_LIST[int(label)]
        path_prefix = f"{prefix}_{artery_prefix}"

        logging.info(f"++++ Starting {path_prefix} Artery Metrics ++++")

        roi = np.zeros_like(vessels_labeled)
        roi[np.where(vessels_labeled == label)] = 1

        roi_markers = np.zeros_like(markers_data)
        roi_markers[np.where(markers_data == label)] = 1

        logging.info("5.1 ++++ Largest Structure Correction ++++")
        with Timer():
            structure = largest_structure(roi)
            markers_structure = largest_structure(roi_markers)

        nib.Nifti1Image(structure, affine, header).to_filename(
            join(args.output, f"{path_prefix}_largest.nii.gz")
        )

        logging.info("5.3 ++++ Analyzing Skeleton ++++")

        with Timer():
            skeleton = analyze_skeleton(structure * center_line)
            marker_skeleton = analyze_skeleton(markers_structure * center_line)
        nib.Nifti1Image(skeleton, affine, header).to_filename(
            join(args.output, f"{path_prefix}_analysed_skl.nii.gz")
        )

        # Determining If a single End point is found in the current artery
        unique, counts = np.unique(skeleton, return_counts=True)

        skl_dict = OrderedDict(zip(unique[1:], counts[1:]))
        if 9 not in skl_dict or (skl_dict[9] < 2 and 7 not in skl_dict):
            logging.info(f"Artery {path_prefix} has insufficient end tags for analysis")
            continue

        if skl_dict[9] == 1 and skl_dict[7] >= 1:
            logging.info(f"Artery {path_prefix} Has Only One End Tags and One Junction tags")
            logging.info(" ===> Converting Junction Tag To End Tag")
            skeleton[skeleton == 7] = 9

        logging.info("5.4 ++++ Finding Willis Start Point ++++")

        with Timer():
            start = find_start_point(marker_skeleton, center)

        logging.info("5.5 ++++ Djikstra's Skeleton Paths Computation ++++")

        with Timer():
            _, indices = find_skeleton_paths(start, skeleton)

        global_indices += indices

    tractogram = Tractogram(streamlines=global_indices, affine_to_rasmm=affine)

    trk_file = TrkFile(tractogram, header)
    trk_file.save(join(args.output, f"{prefix}_perfusing_track.trk"))


def main():
    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(
            format="%(asctime)-15s %(message)s",
            level=logging.INFO if args.verbose == 1 else logging.DEBUG,
        )

    if exists(args.output):
        if not args.overwrite:
            print(f"Outputs directory {args.output} exists. Use -f to for overwriting.")
            sys.exit(1)
    else:
        os.makedirs(args.output)

    logging.info(args)

    labels = nib.load(args.labels)
    labels_data = labels.get_fdata()
    affine = labels.affine
    header = labels.header

    markers_data = nib.load(args.markers).get_fdata()

    center, _ = find_willis_center(markers_data)
    prefix = "TOF"

    binary_labels = np.where(labels_data > 0, 1, 0)
    center_line_data = skeletonize_3d(binary_labels)
    center_line_data = center_line_data.astype(bool).astype(float)

    nib.Nifti1Image(center_line_data, affine, header).to_filename(
        join(args.output, f"{prefix}_center_line_2.nii.gz")
    )

    compute_arterial_metrics_full(
        markers_data,
        labels_data,
        center_line_data,
        center,
        affine,
        header,
        args,
        prefix,
    )


if __name__ == "__main__":
    main()
