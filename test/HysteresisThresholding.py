import logging

from vessel_segmentation.tof_master import hysteresis_thresholding_brain
import argparse
import sys
import os
from os.path import join, exists
import nibabel as nib


def parse_arguments():

    parser = argparse.ArgumentParser(
        description="Segment blood vessels from a VED image using a mixture of Gaussians clustering algorithm and hysteresis approach"
    )

    # Adding command line arguments
    parser.add_argument("output", help="output file name", type=str)
    parser.add_argument("VED", help="ToF MRA VED image [3D image]", type=str)
    parser.add_argument("mask", help="ToF MRA binary mask [3D image]", type=str)
    parser.add_argument("--low", help="Low threshold", type=float, default=3)
    parser.add_argument("--high", help="High threshold", type=float, default=0.5)
    parser.add_argument("--threeD", help="flag to use a 3D structure", action="store_true")
    parser.add_argument("-p", "--prefix", help="output file prefix.")
    parser.add_argument(
        "-f", "--overwrite", help="Force overwriting of the output files.", action="store_true"
    )
    parser.add_argument(
        "-s",
        "--simple_thresh",
        help="If flag , do a simple threshold with low value",
        action="store_true",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="If flagged progress is reported, use -vv for more informations.",
        action="count",
    )

    args = parser.parse_args()

    return args


def main():
    args = parse_arguments()

    if exists(args.output):
        if not args.overwrite:
            print(f"Outputs directory {args.output} exists. Use -f to for overwriting.")
            sys.exit(1)
    else:
        os.makedirs(args.output)

    logging.info("~ =============> HysteresisThresholding <============= ~")
    logging.info(f"Output: {args.output}")
    logging.info(f"VED: {args.VED}")
    logging.info(f"Mask: {args.mask}")
    logging.info(f"Low: {args.low}")
    logging.info(f"High: {args.high}")
    logging.info(f"Is 3D ?: {args.threeD}")
    logging.info(f"Prefix: {args.prefix}")
    logging.info(f"Overwrite: {args.overwrite}")
    logging.info(f"Is simple threshold ?: {args.simple_thresh}")
    logging.info(f"Overwrite: {args.overwrite}")
    logging.info(f"Verbose: {args.verbose}")

    vessels = args.VED
    vessels_img = nib.load(vessels)
    vessels_data = vessels_img.get_fdata("unchanged")
    mask = args.mask
    mask_img = nib.load(mask)
    mask_data = mask_img.get_fdata("unchanged")
    low_t = args.low
    high_t = args.high
    prefix = args.prefix

    if prefix is None:
        file_path = os.path.basename(args.VED)
        prefix = file_path.replace(".gz", "").replace(".nii", "")

    if args.threeD:
        hyst_file = join(
            args.output,
            f"{prefix}_l{low_t}_h{high_t}_3D_hyst_simple_{int(args.simple_thresh)}.nii.gz",
        )
    else:
        hyst_file = join(
            args.output,
            f"{prefix}_l{low_t}_h{high_t}_2D_hyst_simple_{int(args.simple_thresh)}.nii.gz",
        )

    hyst = hysteresis_thresholding_brain(
        vessels_data,
        mask_data,
        is_3d=args.threeD,
        low_factor=low_t,
        high_factor=high_t,
        simple_thres=args.simple_thresh,
    )

    nib.Nifti1Image(hyst, vessels_img.affine, vessels_img.header).to_filename(hyst_file)


if __name__ == "__main__":
    main()
