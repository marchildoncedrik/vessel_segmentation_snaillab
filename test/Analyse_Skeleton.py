#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import logging
import os
import sys
from os.path import exists, join

import nibabel as nib
import numpy as np

from vessel_segmentation import ARTERIAL_MARKER_LIST
from vessel_segmentation.arterial_vessel import analyze_skeleton
from vessel_segmentation.tof_master import centerline_transform
from vessel_segmentation.postprocessing import largest_structure


def parse_arguments() -> argparse.Namespace:
    """Simple CommandLine argument parsing function making use of the argsparse module.

    Returns:
        argparse.Namespace: parsed arguments object args.
    """
    parser = argparse.ArgumentParser(
        description="Return slab points, junction points, adjacent junction points and end points"
        "of labeled arteries images"
    )
    parser.add_argument("output", help="output file name", type=str)
    parser.add_argument("labels", help="ToF MRA labels [ 3D image | .nii/ .nii.gz ]")
    parser.add_argument("prefix", help="Output prefix to save files.")
    parser.add_argument(
        "--tag", help="Tag put in the output file names", default="CerebralArteries"
    )
    parser.add_argument(
        "-f",
        "--overwrite",
        help="Force overwriting of the output files.",
        action="store_true",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="If flagged progress is reported, use -vv for more information.",
        action="count",
    )

    return parser.parse_args()


def main():
    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(
            format="%(asctime)-15s %(message)s",
            level=logging.INFO if args.verbose == 1 else logging.DEBUG,
        )

    if exists(args.output):
        if not args.overwrite:
            print(f"Outputs directory {args.output} exists. Use -f to for overwriting.")
            sys.exit(1)
    else:
        os.makedirs(args.output)

    logging.info("~ =============> Analyse_Skeleton <============= ~")
    logging.info(f"Output: {args.output}")
    logging.info(f"Labels: {args.labels}")
    logging.info(f"Prefix: {args.prefix}")
    logging.info(f"Tag: {args.tag}")
    logging.info(f"Overwrite: {args.overwrite}")
    logging.info(f"Verbose: {args.verbose}")

    labels_img = nib.load(args.labels)
    labels_data = labels_img.get_fdata()

    centerline = centerline_transform((labels_data > 0).astype(float))

    for label in np.unique(labels_data)[1:]:
        logging.info(f"Analyzing skeleton of {ARTERIAL_MARKER_LIST[int(label)]}")

        roi = (labels_data == label).astype(np.int)
        structure = largest_structure(roi)
        skeleton = analyze_skeleton(structure * centerline)

        tagged_skl = analyze_skeleton(skeleton)

        nib.nifti1.Nifti1Image(tagged_skl, labels_img.affine, labels_img.header).to_filename(
            join(
                args.output,
                f"{args.prefix}_{args.tag}_{ARTERIAL_MARKER_LIST[int(label)]}_tagged_skl.nii.gz",
            )
        )


if __name__ == "__main__":
    main()
