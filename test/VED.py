from vessel_segmentation.tof_master import extract_vessels_ved
import argparse
import logging
import os
from os.path import join, exists
import sys
import nibabel as nib


def parse_arguments() -> argparse.Namespace:
    """Simple CommandLine argument parsing function making use of the argsparse module.

    Returns:
        argparse.Namespace: command line arguments.
    """
    parser = argparse.ArgumentParser(description=" Vascular Medical Image Automatic Analysis.")
    parser.add_argument(
        "TOF",
        help="ToF MRA image [ 3D image | .nii/ .nii.gz ] or directory holding ToF Images for a "
        "number of subjects images must contain *TOF/CTA*.nii.gz and can all be in the "
        "input directory or in its subdirectories.",
    )
    parser.add_argument("MASK", help="Brain mask of TOF image")
    parser.add_argument(
        "output",
        help="Defines the output folder. Notice if you use this parameter keep in mind that some "
        "nifti header could differ with the afni processing.",
    )
    parser.add_argument(
        "--sigma_min", help="smallest structure in the TOF image", type=float, default=0.3
    )
    parser.add_argument(
        "--sigma_max", help="largest structure in the TOF image", type=float, default=6
    )
    parser.add_argument("--num_scale", help="number of scales to consider ", type=int, default=20)
    parser.add_argument("--iterations", help="number of iterations", type=int, default=1)
    parser.add_argument("-p", "--prefix", help="output file prefix.")
    parser.add_argument(
        "-f",
        "--overwrite",
        help="Force overwriting of the output files.",
        action="store_true",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="If flagged progress is reported, use -vv for more information.",
        action="count",
    )
    return parser.parse_args()


def main():
    args = parse_arguments()

    if exists(args.output):
        if not args.overwrite:
            print(f"Outputs directory {args.output} exists. Use -f to for overwriting.")
            sys.exit(1)
    else:
        os.makedirs(args.output)

    if args.verbose:
        # TODO: I don't know why but the logging is instanciated before and we can't change it
        # with logging.baseConfig(...).
        fmt = logging.Formatter("%(asctime)-15s %(message)s", None, "%")
        for h in logging.root.handlers:
            h.setFormatter(fmt)
        logging.root.setLevel(logging.INFO if args.verbose == 1 else logging.DEBUG)

    logging.info("~ =============> VED <============= ~")
    logging.info(f"TOF: {args.TOF}")
    logging.info(f"MASK: {args.MASK}")
    logging.info(f"Output: {args.output}")
    logging.info(f"Sigma Min: {args.sigma_min}")
    logging.info(f"Sigma Max: {args.sigma_max}")
    logging.info(f"Number of Scale: {args.num_scale}")
    logging.info(f"Iterations: {args.iterations}")
    logging.info(f"Prefix: {args.prefix}")
    logging.info(f"Overwrite: {args.overwrite}")
    logging.info(f"Verbose: {args.verbose}")

    tof_img = nib.load(args.TOF)
    affine = tof_img.affine
    header = tof_img.header

    mask_img = nib.load(args.MASK)
    mask_data = mask_img.get_fdata("unchanged")

    prefix = args.prefix
    if prefix is None:
        file_path = os.path.basename(args.TOF)
        prefix = file_path.replace(".gz", "").replace(".nii", "")

    vesselness_file = join(
        args.output,
        f"{prefix}_m{args.sigma_min}_M{args.sigma_max}_ns{args.num_scale}_i{args.iterations}_Ved.nii.gz",
    )
    ved = extract_vessels_ved(
        input_image=args.TOF,
        output_image=vesselness_file,
        mask=mask_data,
        output=args.output,
        sigma_min=args.sigma_min,
        sigma_max=args.sigma_max,
        num_scale=args.num_scale,
        iteration=args.iterations,
    )

    nib.Nifti1Image(ved, affine, header).to_filename(vesselness_file)


if __name__ == "__main__":
    main()
