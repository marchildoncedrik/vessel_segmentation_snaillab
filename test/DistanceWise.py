#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import os
import sys
from collections import OrderedDict
import logging
from os.path import join, basename, exists
from typing import Sequence

import nibabel as nib
import numpy as np
import pandas as pd
from skimage.morphology import skeletonize_3d

from vessel_segmentation import ARTERIAL_MARKER_LIST
from vessel_segmentation.arterial_vessel import (
    analyze_skeleton,
    compute_distance_wise_diameter,
    compute_vascular_distance,
    diameter_transform,
    find_main_trunk,
    find_skeleton_paths,
    find_start_point,
    find_true_skeleton,
    generate_orders_map,
    generate_frequency_path,
    largest_structure,
    find_longest_path,
    reduce_wb_analysis,
)
from vessel_segmentation.Timer import Timer
from vessel_segmentation.tof_master import find_willis_center


def parse_arguments() -> argparse.Namespace:
    """Simple CommandLine argument parsing function making use of the argsparse module.

    Returns:
        argparse.Namespace: parsed arguments object args.
    """
    parser = argparse.ArgumentParser(description="Distance Wise analysis")
    parser.add_argument("output", help="Output directory.")
    parser.add_argument("labels", help="Segmentation labels. [ 3D image | .nii/ .nii.gz ]")
    parser.add_argument("markers", help="Circle of willis markers. [ 3D image | .nii/ .nii.gz ]")
    parser.add_argument("tof", help="ToF MRA iso image. [ 3D image | .nii/ .nii.gz ]")
    parser.add_argument("--step", help="Step for diameter computation.", type=float, default=1)
    parser.add_argument(
        "--tag", help="Tag put in the output file names", default="CerebralArteries"
    )
    parser.add_argument(
        "-m",
        "--main_trunk_stop",
        help="Choose a value between 0-1 to early stop main trunk",
        type=float,
        default=0.65,
    )
    parser.add_argument(
        "-d",
        "--diameters",
        help="Diameter image. [ 3D image | .nii/ .nii.gz ]",
    )
    parser.add_argument(
        "-r",
        "--reduced_analysis",
        help="Flag to have a reduced_value analysis",
        action="store_true",
    )
    parser.add_argument(
        "-f",
        "--overwrite",
        help="Force overwriting of the output files.",
        action="store_true",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="If flagged progress is reported, use -vv for more informations.",
        action="count",
    )

    args = parser.parse_args()

    return args


def compute_output(
    args,
    structure,
    center_line,
    markers_structure,
    path_prefix,
    affine,
    header,
    center,
    resampled_data,
    roi,
    diameter,
    dfs_reduced,
    artery_prefix,
    comments="",
):
    logging.info("5.3 ++++ Analyzing Skeleton ++++")

    with Timer():
        skeleton = analyze_skeleton(structure * center_line)
        marker_skeleton = analyze_skeleton(markers_structure * center_line)
    nib.Nifti1Image(skeleton, affine, header).to_filename(
        join(args.output, f"{path_prefix}_analysed_skl{comments}.nii.gz")
    )

    # Determining If a single End point is found in the current artery
    unique, counts = np.unique(skeleton, return_counts=True)

    skl_dict = OrderedDict(zip(unique[1:], counts[1:]))
    if 9 not in skl_dict or (skl_dict[9] < 2 and 7 not in skl_dict):
        logging.info(f"Artery {path_prefix} has insufficient end tags for analysis")
        return None

    if skl_dict[9] == 1 and skl_dict[7] >= 1:
        logging.info(f"Artery {path_prefix} Has Only One End Tags and One Junction tags")
        logging.info(" ===> Converting Junction Tag To End Tag")
        skeleton[skeleton == 7] = 9

    logging.info("5.4 ++++ Finding Willis Start Point ++++")

    with Timer():
        start = find_start_point(marker_skeleton, center)

    logging.info("5.5 ++++ Djikstra's Skeleton Paths Computation ++++")

    with Timer():
        costs, indices = find_skeleton_paths(start, skeleton)

    logging.info("5.6 ++++ Computing True Skeleton ++++")

    with Timer():
        true_skeleton = find_true_skeleton(indices, skeleton.shape)
    nib.Nifti1Image(true_skeleton, affine, header).to_filename(
        join(args.output, f"{path_prefix}_skl_true{comments}.nii.gz")
    )

    logging.info("5.7 ++++ Computing Frequency Path ++++")

    with Timer():
        frequency = generate_frequency_path(indices, skeleton.shape)

    nib.Nifti1Image(frequency, affine, header).to_filename(
        join(args.output, f"{path_prefix}_skl_frequency{comments}.nii.gz")
    )

    logging.info("5.8 ++++ Computing Vascular Distance & Mask Distance to Vessel & Travelled ++++")

    with Timer():
        (
            distance,
            tortuosity,
            angulation,
            _,
            _,
        ) = compute_vascular_distance(roi, indices, frequency, header.get_zooms())
    nib.Nifti1Image(distance, affine, header).to_filename(
        join(args.output, f"{path_prefix}_skl_distance{comments}.nii.gz")
    )
    nib.Nifti1Image(tortuosity, affine, header).to_filename(
        join(args.output, f"{path_prefix}_skl_tortuosity{comments}.nii.gz")
    )
    nib.Nifti1Image(angulation, affine, header).to_filename(
        join(args.output, f"{path_prefix}_skl_angulation{comments}.nii.gz")
    )

    logging.info("5.9 ++++ Computing Main Trunk ++++")

    with Timer():
        main_trunk = find_main_trunk(
            frequency, start, distance=distance, main_trunk_stop=args.main_trunk_stop
        )

    nib.Nifti1Image(main_trunk, affine, header).to_filename(
        join(args.output, f"{path_prefix}_skl_main{comments}.nii.gz")
    )

    logging.info("5.10 ++++ Computing Longest Path ++++")

    with Timer():
        longest_path = find_longest_path(
            indices=indices, skeleton_shape=skeleton.shape, distance=distance
        )

    nib.Nifti1Image(longest_path, affine, header).to_filename(
        join(args.output, f"{path_prefix}_skl_longest{comments}.nii.gz")
    )

    logging.info("5.10.1 ++++ Computing Orders ++++")

    with Timer():
        orders = generate_orders_map(frequency, main_trunk, indices)

    nib.Nifti1Image(orders, affine, header).to_filename(
        join(args.output, f"{path_prefix}_skl_orders{comments}.nii.gz")
    )
    dataframe = reduce_wb_analysis(orders, distance, longest_path, indices, diameter.copy())
    dfs_reduced.append((artery_prefix, dataframe))

    if not args.reduced_analysis:
        df_dict = {
            "Intensity": resampled_data.copy(),
            "Diameter": diameter.copy(),
            "Tortuosity": tortuosity.copy(),
            "Angulation": angulation.copy(),
        }

        for k, v in df_dict.items():
            logging.info("\n   +++: %s \n" % (k,))
            df = compute_distance_wise_diameter(main_trunk, distance, v, args.step)
            df.to_csv(join(args.output, f"{path_prefix}_Distance_wise_{k}_Main{comments}.csv"))
            logging.info(df)

    return dfs_reduced


def compute_arterial_metrics_full(
    markers_data: np.ndarray,
    resampled_data: np.array,
    vessels_labeled: np.ndarray,
    center_line: np.ndarray,
    diameter: np.ndarray,
    center: Sequence[float],
    affine: np.ndarray,
    header: nib.Nifti1Header,
    args: argparse.Namespace,
    prefix: str,
    brain_vasculature: str = "CerebralArteries",
):
    """Compute arterial metrics for each artery in the brain.

    Args:
        markers_data (np.ndarray): Markers segmentation.
        resampled_data (np.array): TOF image.
        vessels_labeled (np.array): Image of the vessels with a tag.
        center_line (np.ndarray): Center line image of the vessels labeled.
        diameter (np.ndarray): Diameter image of the vessels labeled.
        center (Sequence[float]): Center of mass of the markers.
        affine (np.ndarray): Reference affine.
        header (nib.Nifti1Header): Reference header.
        args (argparse.Namespace): Arguments from command line
        prefix (str): Use as prefix of filename.
        brain_vasculature (str, optional): Defaults to "CerebralArteries".

    """

    logging.info("3. ===> Computing Arterial Metrics <===")

    dfs_reduced = []

    for label in np.unique(markers_data)[1:]:
        if label > len(ARTERIAL_MARKER_LIST):
            logging.warning(f"Label {label} isn't an identified artery!")
            continue

        artery_prefix = ARTERIAL_MARKER_LIST[int(label)]

        path_prefix = f"{prefix}_{brain_vasculature}_{artery_prefix}"

        logging.info(f"++++ Starting {path_prefix} Artery Metrics ++++")

        roi = np.zeros_like(vessels_labeled)
        roi[np.where(vessels_labeled == label)] = 1

        roi_markers = np.zeros_like(markers_data)
        roi_markers[np.where(markers_data == label)] = 1

        logging.info("5.1 ++++ Largest Structure Correction ++++")
        with Timer():
            structure = largest_structure(roi)
            markers_structure = largest_structure(roi_markers)
            intensity_data = structure * resampled_data

        nib.Nifti1Image(structure, affine, header).to_filename(
            join(args.output, f"{path_prefix}_largest.nii.gz")
        )

        nib.Nifti1Image(intensity_data, affine, header).to_filename(
            join(args.output, f"{path_prefix}_intensity_scaled.nii.gz")
        )

        dfs_reduced = compute_output(
            args,
            structure,
            center_line,
            markers_structure,
            path_prefix,
            affine,
            header,
            center,
            resampled_data,
            roi,
            diameter,
            dfs_reduced,
            artery_prefix,
            comments="",
        )

    metrics = OrderedDict(dfs_reduced)
    pd.DataFrame(metrics).to_csv(join(args.output, f"{prefix}_{brain_vasculature}_SUMMARY.csv"))


def main():
    args = parse_arguments()
    if not (0 <= args.main_trunk_stop <= 1):
        print(f"main_trunk_stop must be between 0 - 1, got {args.main_trunk_stop}")
        sys.exit(1)

    if args.verbose:
        logging.basicConfig(
            format="%(asctime)-15s %(message)s",
            level=logging.INFO if args.verbose == 1 else logging.DEBUG,
        )

    if exists(args.output):
        if not args.overwrite:
            print(f"Outputs directory {args.output} exists. Use -f to for overwriting.markers_data")
            sys.exit(1)
    else:
        os.makedirs(args.output)

    logging.info(args)

    logging.info("~ =============> DistanceWise <============= ~")
    logging.info(f"Output: {args.output}")
    logging.info(f"Labels: {args.labels}")
    logging.info(f"Markers: {args.markers}")
    logging.info(f"TOF: {args.tof}")
    logging.info(f"Step: {args.step}")
    logging.info(f"Main trunk stop: {args.main_trunk_stop}")
    logging.info(f"Diameters: {args.diameters}")
    logging.info(f"Reduced analysis: {args.reduced_analysis}")
    logging.info(f"Overwrite: {args.overwrite}")
    logging.info(f"Verbose: {args.verbose}")

    markers_data = nib.load(args.markers).get_fdata()
    labels_data = nib.load(args.labels).get_fdata()

    tof_img = nib.load(args.tof)
    tof_data = tof_img.get_fdata()
    affine = tof_img.affine
    header = tof_img.header

    center, _ = find_willis_center(markers_data)
    prefix = basename(args.tof).replace(".gz", "").replace(".nii", "")
    brain_vasculature = args.tag

    binary_labels = np.where(labels_data > 0, 1, 0)
    center_line_data = skeletonize_3d(binary_labels)
    center_line_data = center_line_data.astype(bool).astype(float)

    if not args.diameters:
        logging.info("+++ Extracting diameters +++")
        diameter_data = diameter_transform(binary_labels, tof_img.header.get_zooms())
    else:
        diameter_data = nib.load(args.diameters).get_fdata()
        assert (
            diameter_data.shape == markers_data.shape
        ), "Wrong diameters image dimension. Resample to markers dimension"

    nib.Nifti1Image(center_line_data, affine, header).to_filename(
        join(args.output, f"{prefix}_center_line_2.nii.gz")
    )

    logging.info(" ===> Normalizing with the circle of Willis")
    tof_array_norm = tof_data[np.where(markers_data != 0)]
    upper_quantile = np.quantile(tof_array_norm, 0.99)

    tof_data /= upper_quantile

    compute_arterial_metrics_full(
        markers_data,
        tof_data,
        labels_data,
        center_line_data,
        diameter_data,
        center,
        affine,
        header,
        args,
        prefix,
        brain_vasculature,
    )


if __name__ == "__main__":
    main()
